<?php
/**
 * Template Name: No Sidebar Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Mystery Themes
 * @subpackage Editorial
 * @since 1.0.0
 */
get_header(); ?>

	<div class="featured-slider-section clearfix">
		
			<?php
	        	if( is_active_sidebar( 'editorial_technology_content_area' ) ) {
	            	if ( !dynamic_sidebar( 'editorial_technology_content_area' ) ):
	            	endif;
	         	}
	        ?>
	</div><!-- .featured-slider-section -->
<?php
get_footer();