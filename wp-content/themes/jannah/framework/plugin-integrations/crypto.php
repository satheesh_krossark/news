<?php
/**
 * Cryptocurrency All-in-One
 * WP Ultimate Crypto
 *
 * @package Jannah
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly



/*-----------------------------------------------------------------------------------*/
# Wrap the Shortcode of the Cryptocurrency All-in-One and remove the H2 title tag
/*-----------------------------------------------------------------------------------*/
if ( JANNAH_CRYPTOALL_IS_ACTIVE ){

	add_filter( 'do_shortcode_tag','add_my_script', 10, 3 );
	function add_my_script($output, $tag, $attr){

		if( $tag != 'currencyprice' ){
			return $output;
		}

		$output = preg_replace( '/<h2\b[^>]*>(.*?)<\/h2>/i', '', $output );
		$output = str_replace( '="', 'xyz', $output );
		$output = str_replace( '= jQuery', 'xx jQuery', $output );
		$output = str_replace( '=', '</span><span class="bct-equal-sign">=</span>', $output );
		$output = str_replace( 'xyz', '="', $output );
		$output = str_replace( 'xx jQuery', '= jQuery', $output );
		$output = str_replace( '<input type="text" class="currency1value" value="1" />', '<input type="text" class="currency1value" value="1" /><span class="bct-currency-1">', $output );

		return '<div class="btc-calculator">'. $output .'</div>';
	}
}

