<?php
/**
 * Load the plugin integrations functions
 *
 * @package Jannah
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


$plugin_file_path = 'framework/plugin-integrations/';



/*
 * AMP
 *
 * By: Automattic
 * https://wordpress.org/plugins/amp/
 */
 locate_template( $plugin_file_path . 'amp.php', true, true );


/*
 * WooCommerce
 *
 * By: Automattic
 * https://wordpress.org/plugins/woocommerce/
 */
 locate_template( $plugin_file_path . 'woocommerce.php', true, true );


/*
 * Sensei
 *
 * By: Automattic
 * https://woocommerce.com/products/sensei/
 */
 locate_template( $plugin_file_path . 'sensei.php', true, true );


/*
 * BuddyPress
 *
 * By: Multiple Authors
 * https://wordpress.org/plugins/buddypress/
 */
 locate_template( $plugin_file_path . 'buddypress.php', true, true );


/*
 * Jetpack
 *
 * By: Automattic
 * https://wordpress.org/plugins/jetpack/
 */
 locate_template( $plugin_file_path . 'jetpack.php', true, true );


 /*
  * Instant Articles for WP
  *
  * By: Automattic, Dekode, Facebook
  * https://wordpress.org/plugins/fb-instant-articles/
  */
  locate_template( $plugin_file_path . 'fb-instant-articles.php', true, true );


 /*
  * Cryptocurrency All-in-One
  * WP Ultimate Crypto
  *
  */
 locate_template( $plugin_file_path . 'crypto.php', true, true );
