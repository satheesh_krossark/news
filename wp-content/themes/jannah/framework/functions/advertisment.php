<?php
/**
 * Social functions
 *
 * @package Jannah
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly



/*-----------------------------------------------------------------------------------*/
# Ads
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_get_banner' )){

	function jannah_get_banner( $banner, $before = false, $after = false, $echo = true ){

		# Check if the banner is disabled or hidden on mobiles ----------
		if( ! jannah_get_option( $banner ) || jannah_is_mobile_and_hidden( $banner ) ) return;

		$the_ad = '';

		# Get the banner ----------
		$the_ad .= $before;

		# Ad Title ----------
		if( jannah_get_option( $banner.'_title' ) ){

			$the_ad .= jannah_get_option( $banner.'_title_link' ) ? '<a title="'. esc_attr( jannah_get_option( $banner.'_title' ) ) .'" href="'. esc_attr( jannah_get_option( $banner.'_title_link' ) ) .'" rel="nofollow" target="_blank" class="stream-title">' : '<span class="stream-title">';
			$the_ad .= jannah_get_option( $banner.'_title' );
			$the_ad .= jannah_get_option( $banner.'_title_link' ) ? '</a>' : '</span>';
		}


		# Ad Rotate ----------
		if( jannah_get_option( $banner.'_adrotate' ) && function_exists( 'adrotate_ad' )){

			$adrotate_id = jannah_get_option( $banner.'_adrotate_id' ) ? jannah_get_option( $banner.'_adrotate_id' ) : '';

			if( jannah_get_option( $banner.'_adrotate_type' ) == 'group' && function_exists( 'adrotate_group' )){
				$the_ad .= adrotate_group( $adrotate_id, 0, 0, 0);
			}
			elseif( jannah_get_option( $banner.'_adrotate_type' ) == 'single' ){
				$the_ad .= adrotate_ad( $adrotate_id, true, 0, 0, 0);
			}
		}

		# Custom Code ----------
		elseif( $code = jannah_get_option( $banner.'_adsense' ) ){

			$code = str_replace( 'data-ad-format="auto"', '', $code );

			$the_ad .= do_shortcode( $code );
		}

		# Image ----------
		elseif( $img = jannah_get_option( $banner.'_img' ) ){

			$target   = jannah_get_option( $banner.'_tab' ) ? 'target="_blank"' : '';
			$nofollow = jannah_get_option( $banner.'_nofollow' ) ? 'rel="nofollow"' : '';
			$title    = jannah_get_option( $banner.'_alt' ) ? jannah_get_option( $banner.'_alt' ) : '';
			$url      = apply_filters( 'jannah_ads_url', jannah_get_option( $banner.'_url' ) ? jannah_get_option( $banner.'_url' ) : '' );

			$the_ad .= '
				<a href="'. esc_url( $url ) .'" title="'. esc_attr( $title ).'" '. $target .' '. $nofollow .'>
					<img src="'. esc_url( $img ) .'" alt="'. esc_attr( $title ).'" width="728" height="90" />
				</a>
			';
		}

		$the_ad .= ( $after );

		# Print the Ad ----------
		if( $echo ){
			echo $the_ad;
		}

		return $the_ad;
	}

}



/*-----------------------------------------------------------------------------------*/
# Ad Blocks Message
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'tie_ad_blocker_msg' )){

	add_action( 'wp_footer', 'tie_ad_blocker_msg' );
	function tie_ad_blocker_msg(){

		if( jannah_get_option( 'ad_blocker_detector' ) ){
			?>
			<div id="tie-popup-adblock" class="tie-popup is-fixed-popup">
				<div class="tie-popup-container">
					<div class="container-wrapper">
					<span class="fa fa-ban" aria-hidden="true"></span>
					<h2><?php _eti( 'Adblock Detected' ) ?></h2>
					<div class="adblock-message"><?php _eti( 'Please consider supporting us by disabling your ad blocker' ) ?></div>
					</div><!-- .container-wrapper  /-->
				</div><!-- .tie-popup-container /-->
			</div><!-- .tie-popup /-->
			<script type='text/javascript' src='<?php echo JANNAH_TEMPLATE_URL ?>/js/advertisement.js'></script>
			<?php
		}
	}

}



/*-----------------------------------------------------------------------------------*/
# Background Ad
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_ads_background' )){

	add_action( 'jannah_before_wrapper', 'jannah_ads_background' );
	function jannah_ads_background(){
		if( jannah_get_option( 'banner_bg' ) && jannah_get_option( 'banner_bg_url' ) ){
			echo '<a id="background-ad-cover" href="'. esc_url( jannah_get_option('banner_bg_url') ) .'" target="_blank" rel="nofollow"></a>';
		}
	}

}



/*-----------------------------------------------------------------------------------*/
# Above Post Ad
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_above_post_ad' )){

	function jannah_above_post_ad(){
		if( ! jannah_get_postdata( 'tie_hide_above' )){
			if( jannah_get_postdata( 'tie_get_banner_above' )){
				echo '<div class="stream-item stream-item-above-post">'. do_shortcode( jannah_get_postdata( 'tie_get_banner_above' )) .'</div>';
			}
			else{
				jannah_get_banner( 'banner_above', '<div class="stream-item stream-item-above-post">', '</div>' );
			}
		}
	}

}



/*-----------------------------------------------------------------------------------*/
# Below Post Ad
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_below_post_ad' )){

	function jannah_below_post_ad(){
		if( ! jannah_get_postdata( 'tie_hide_below' )){
			if( jannah_get_postdata( 'tie_get_banner_below' )){
				echo '<div class="stream-item stream-item-below-post">'. do_shortcode( jannah_get_postdata( 'tie_get_banner_below' )) .'</div>';
			}
			else{
				jannah_get_banner( 'banner_below', '<div class="stream-item stream-item-below-post">', '</div>' );
			}
		}
	}

}



/*-----------------------------------------------------------------------------------*/
# Above Post Content Ad
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_above_post_content_ad' )){

	function jannah_above_post_content_ad(){
		if( ! jannah_get_postdata( 'tie_hide_above_content' )){
			if( jannah_get_postdata( 'tie_get_banner_above_content' )){
				echo '<div class="stream-item stream-item-above-post-content">'. do_shortcode( jannah_get_postdata( 'tie_get_banner_above_content' )) .'</div>';
			}
			else{
				jannah_get_banner( 'banner_above_content', '<div class="stream-item stream-item-above-post-content">', '</div>' );
			}
		}
	}

}



/*-----------------------------------------------------------------------------------*/
# Below Post Content Ad
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_below_post_content_ad' )){

	function jannah_below_post_content_ad(){
		if( ! jannah_get_postdata( 'tie_hide_below_content' )){
			if( jannah_get_postdata( 'tie_get_banner_below_content' )){
				echo '<div class="stream-item stream-item-below-post-content">'. do_shortcode( jannah_get_postdata( 'tie_get_banner_below_content' )) .'</div>';
			}
			else{
				jannah_get_banner( 'banner_below_content', '<div class="stream-item stream-item-below-post-content">', '</div>' );
			}
		}
	}

}





/*-----------------------------------------------------------------------------------*/
# Below Post Content Ad
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'tie_add_post_after_x_posts' )){

	add_action( 'tie_after_post_in_archives', 'tie_add_post_after_x_posts', 10, 2 );
	function tie_add_post_after_x_posts( $layout, $latest_count ){

		# Ads tags ---------
		if( $layout == 'overlay' || $layout == 'overlay-spaces' || $layout == 'masonry' ){

			$before_ad = '<div class="post-element stream-item stream-item-between stream-item-between-2">';
			$after_ad  = '</div>';
		}
		else{
			$before_ad = '<li class="post-item stream-item stream-item-between stream-item-between-2"><div class="post-item-inner">';
			$after_ad  = '</div></li>';
		}


		# Get the ADs ----------
		if( $latest_count == jannah_get_option( 'between_posts_1_posts_number' ) ){

			jannah_get_banner( 'between_posts_1', $before_ad, $after_ad );
		}

		if( $latest_count == jannah_get_option( 'between_posts_2_posts_number' ) ){
			jannah_get_banner( 'between_posts_2', $before_ad, $after_ad );
		}

	}

}


/*

	add_filter( 'the_content', 'jannah_insert_after_paragraph', 9 );

	function jannah_insert_after_paragraph( $content ) {

		if ( is_single() && ! is_admin() ) {

			$content_parts   = preg_split('/(<p.*>)/U', $content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			$content_new     = '';
			$paragraph_count = 0;

			foreach( $content_parts as $content_part_index => $content_part_value) {

				if( ! empty( $content_part_value ) ){
					if (preg_match('/(<p.*>)/U', $content_part_value) === 1) {
						if ( 13 == $paragraph_count ) {
							$content_new .=  '<span style="color: red">---- THIS IS AD ----</span>';
						}

						$paragraph_count++;
					}
				}

				$content_new .= $content_part_value;
			}

			$content = $content_new;
		}

		return $content;
	}

*/
