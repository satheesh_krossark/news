<?php
/**
 * Weather
 *
 * @package Jannah
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly



/*-----------------------------------------------------------------------------------*/
# Get Weather data
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'tie_get_weather' )){

	function tie_get_weather( $atts ){



		# Default Data
		/*----------------------------------------------*/
		$rtn               = '';
		$location          = isset( $atts['location'] ) ? $atts['location'] : false;
		$api_key           = isset( $atts['api_key'] )  ? $atts['api_key']  : false;
		$units             = ( isset( $atts['units'] ) AND strtoupper( $atts['units'] ) == 'C' ) ? 'metric' : 'imperial';
		$units_display     = $units == 'metric' ? '&#x2103;' : '&#x2109;';
		$days_to_show      = isset( $atts['forecast_days'] ) ? $atts['forecast_days'] : 5;
		$is_animated       = ! empty( $atts['animated'] ) ? 'is-animated' : '';
		$available_locales = array( 'en', 'ru', 'it', 'es', 'uk', 'de', 'pt', 'ro', 'pl', 'fi', 'nl', 'fr', 'bg', 'sv', 'zh_tw', 'zh_cn', 'tr', 'hr', 'ca'  );



		# Return if the API key is Missing
		/*----------------------------------------------*/
		if( empty( $api_key ) ){

			echo '
				<div class="weather-wrap">
					<span class="theme-notice">'. esc_html__( 'WEATHER WIDGET: You need to set the API key and Location.', 'jannah' ) .'</span>
				</div>
			';

			return;
		}



		# Return if there is no a location
		/*----------------------------------------------*/
		if( ! $location ){

			return tie_weather_get_error();
		}



		# Set the language
		/*----------------------------------------------*/
		$locale = in_array( get_locale(), $available_locales ) ? get_locale() : 'en';

		# Check for locale by first two digits
		if( in_array( substr( get_locale(), 0, 2 ), $available_locales )){

			$locale = substr( get_locale(), 0, 2 );
		}



		# Prepare the location and the queiry
		/*----------------------------------------------*/
		$city_name_slug = is_numeric( $location ) ? $location : sanitize_title( $location );
		$api_query      = is_numeric( $location ) ? "id=" . $location : "q=" . $location;



		# Transient Name
		/*----------------------------------------------*/
		$transient_name = 'tie_weather_' . $city_name_slug . '_' . strtolower( $units ) . '_' . $locale;



		# Get the stored weather data
		/*----------------------------------------------*/
		$weather_data = get_transient( $transient_name );



		# If no Cached version lets get it
		/*----------------------------------------------*/
		if( ! $weather_data ){

			$weather_data = array(
				'now'      => array(),
				'forecast' => array(),
			);


			# Today's Weather data
			/*----------------------------------------------*/
			$api_url     = jannah_remove_spaces( "http://api.openweathermap.org/data/2.5/weather?" . $api_query . "&lang=" . $locale . "&units=" . $units."&appid=".$api_key );
			$api_connect = wp_remote_get( $api_url, array( 'timeout' => 30 ) );


			# return if there is an error ----------
			if( is_wp_error( $api_connect )){

				return tie_weather_get_error( $api_connect->get_error_message() );
			}

			$weather_data['now'] = json_decode( $api_connect['body'] );

			# return if there is an error ----------
			if( isset( $weather_data['now']->cod ) AND $weather_data['now']->cod == 404 ){

				return tie_weather_get_error( $weather_data['now']->message );
			}



			# Forecast Weather data
			/*----------------------------------------------*/
			$api_url     = jannah_remove_spaces( "https://api.openweathermap.org/data/2.5/forecast?" . $api_query . "&lang=" . $locale . "&units=" . $units ."&appid=".$api_key );
			$api_connect = wp_remote_get( $api_url, array( 'timeout' => 30 ) );

			# return if there is an error ----------
			if( is_wp_error( $api_connect ) ){

				return tie_weather_get_error( $api_connect->get_error_message()  );
			}

			$weather_data['forecast'] = json_decode( $api_connect['body'] );

			# return if there is an error ----------
			if( isset( $weather_data['forecast']->cod ) AND $weather_data['forecast']->cod == 404 ){

				return tie_weather_get_error( $weather_data['forecast']->message );
			}



			# Set the transient, cache for two hours
			/*----------------------------------------------*/
			if( $weather_data['now'] AND $weather_data['forecast'] ){
				set_transient( $transient_name, $weather_data, 2 * HOUR_IN_SECONDS );
			}
		}



		# return if there is no Weather data ----------
		if( ! $weather_data OR ! isset( $weather_data['now'] ) ){
			return tie_weather_get_error();
		}



		# Today's data
		/*----------------------------------------------*/
		$today      = $weather_data['now'];
		$city_name  = ! empty( $atts['custom_name'] ) ? $atts['custom_name'] : $today->name;

		# Today's Icon
		$today_icon = ! empty( $atts['debug'] ) ? $atts['debug'] : $today->weather[0]->icon;
		$icon_divs  = tie_weather_status_icon( $today_icon );

		# Today's weather data
		$today_temp = isset( $today->main->temp )     ? round( $today->main->temp )     : false;
		$today_high = isset( $today->main->temp_max ) ? round( $today->main->temp_max ) : false;
		$today_low 	= isset( $today->main->temp_min ) ? round( $today->main->temp_min ) : false;



		# Forecast's data
		/*----------------------------------------------*/
		$forecast_days = array();
		$forecast_out  = '';
		$today_date    = date( 'Ymd', current_time( 'timestamp', 0 ) );

		if( ! empty( $weather_data['forecast'] ) && ! empty( $weather_data['forecast']->list ) ){

			# The Api Returns 5 day / 3 hour forecast data so we need to collapse them and
			foreach( (array) $weather_data['forecast']->list as $forecast ){

				$day_of_week = date( 'Ymd', $forecast->dt );

				# Days after today only ----------
				if( $today_date > $day_of_week ) continue;


				# If it is today lets get the max and min ----------
				if( $today_date == $day_of_week ){

					# max ---
					if( ! empty( $forecast->main->temp_max ) && $forecast->main->temp_max > $today_high ){
						$today_high = round( $forecast->main->temp_max );
					}

					# min ---
					if( ! empty( $forecast->main->temp_min ) && $forecast->main->temp_min < $today_low ){
						$today_low = round( $forecast->main->temp_min );
					}
				}

				# Rest Days ----------
				if( empty( $forecast_days[ $day_of_week ] ) ){

					$forecast_days[ $day_of_week ] = array(
						'utc'  => $forecast->dt,
						'icon' => $forecast->weather[0]->icon,
						'temp' => ! empty( $forecast->main->temp_max ) ? round( $forecast->main->temp_max ) : '',
					);
				}
				else{

					# Get the max temp in the day ---
					if( $forecast->main->temp_max > $forecast_days[ $day_of_week ]['temp'] ){
						$forecast_days[ $day_of_week ]['temp'] = round( $forecast->main->temp_max );

						# Chnage the icon of the day to the max icon
						$forecast_days[ $day_of_week ]['icon'] = $forecast->weather[0]->icon;
					}
				}
			}



			# Show the Forecast data
			/*----------------------------------------------*/
			foreach( $forecast_days as $forecast_day ){

				$forecast_icon = tie_weather_status_icon( $forecast_day['icon'] );
				$the_day = date_i18n( 'D', $forecast_day['utc'] );

				$forecast_out .= "
					<div class=\"weather-forecast-day\">
						{$forecast_icon}
						<div class=\"weather-forecast-day-temp\">{$forecast_day['temp']}<sup>{$units_display}</sup></div>
						<div class=\"weather-forecast-day-abbr\">{$the_day}</div>
					</div>
				";
			}
		}



		# Display the weather | NORMAL LAYOUT
		/*----------------------------------------------*/
		if( empty( $atts['compact'] ) ){

			$rtn .= "
				<div id=\"tie-weather-{$city_name_slug}\" class=\"weather-wrap {$is_animated}\">
			";

			$rtn .= "
				<div class=\"weather-icon-and-city\">
					{$icon_divs}
					<h6 class=\"weather-name\">{$city_name}</h6>
					<div class=\"weather-desc\">{$today->weather[0]->description}</div>
				</div> <!-- /.weather-icon-and-city -->
			";


			$speed_text = ($units == 'metric') ? _ti('km/h') : _ti('mph');

			$rtn .= "
				<div class=\"weather-todays-stats\">
					<div class=\"weather-current-temp\">$today_temp<sup>{$units_display}</sup></div>
					<div class=\"weather-more-todays-stats\">";

					if( ! empty( $today_high ) && ! empty( $today_low ) ){
						$rtn .= "
							<div class=\"weather_highlow\"><span aria-hidden=\"true\" class=\"tie-icon-thermometer-half\"></span> {$today_high}&ordm; - {$today_low}&ordm;</div>";
					}

					$rtn .= "
						<div class=\"weather_humidty\"><span aria-hidden=\"true\" class=\"tie-icon-raindrop\"></span><span class=\"screen-reader-text\">" . esc_html__('humidity:', 'jannah') . "</span> {$today->main->humidity}% </div>
						<div class=\"weather_wind\"><span aria-hidden=\"true\" class=\"tie-icon-wind\"></span><span class=\"screen-reader-text\">" . esc_html__('wind:', 'jannah') . "</span> {$today->wind->speed} " . $speed_text . "</div>
					</div>
				</div> <!-- /.weather-todays-stats -->
			";


			#-----
			if( $days_to_show != 'hide' ){
				$rtn .= "
					<div class=\"weather-forecast small-weather-icons weather_days_{$days_to_show}\">
						$forecast_out
					</div><!-- /.weather-forecast -->
				";
			}


			$rtn .= "</div> <!-- /.weather-wrap -->";
		}


		# Display the weather | Comapct LAYOUT
		/*----------------------------------------------*/
		else{

			$rtn .= "
				<div class=\"tie-weather-widget {$is_animated}\" title=\"{$today->weather[0]->description}\">
					<div class=\"weather-wrap\">

						<div class=\"weather-forecast-day small-weather-icons\">
							{$icon_divs}
						</div><!-- .weather-forecast-day -->

						<div class=\"city-data\">
							<span>{$city_name}</span>
						  <span class=\"weather-current-temp\">$today_temp<sup>{$units_display}</sup></span>
						</div><!-- .city-data -->

					</div><!-- .weather-wrap -->
				</div><!-- .tie-weather-widget -->
			";
		}



		return $rtn;
	}



	# Return Weather Status icon
	function tie_weather_status_icon( $today_icon ){

		# Default icon | Cloudy ----------
		$weather_icon = '
			<div class="weather-icon">
        <div class="icon-cloud"></div>
        <div class="icon-cloud-behind"></div>
        <div class="icon-basecloud-bg"></div>
      </div>
		';

		# Sunny ----------
		if( $today_icon == '01d' ){
			$weather_icon = '
				<div class="weather-icon">
					<div class="icon-sun"></div>
				</div>
			';
		}

		# Moon ----------
		elseif( $today_icon == '01n' ){
			$weather_icon = '
				<div class="weather-icon">
					<div class="icon-moon"></div>
				</div>
			';
		}

		# Cloudy Sunny ----------
		elseif( $today_icon == '02d' || $today_icon == '03d' || $today_icon == '04d' ){
			$weather_icon = '
				<div class="weather-icon">
          <div class="icon-cloud"></div>
          <div class="icon-cloud-behind"></div>
          <div class="icon-basecloud-bg"></div>
          <div class="icon-sun-animi"></div>
        </div>
			';
		}

		# Cloudy Night ----------
		elseif( $today_icon == '02n' || $today_icon == '03n'  || $today_icon == '04n' ){
			$weather_icon = '
				<div class="weather-icon">
					<div class="icon-cloud"></div>
					<div class="icon-cloud-behind"></div>
					<div class="icon-basecloud-bg"></div>
					<div class="icon-moon-animi"></div>
				</div>
			';
		}

		# Showers ----------
		elseif( $today_icon == '09d' ||  $today_icon == '09n'){
			$weather_icon = '
				<div class="weather-icon drizzle-icons showers-icons">
					<div class="basecloud"></div>
					<div class="animi-icons-wrap">
						<div class="icon-rainy-animi"></div>
						<div class="icon-rainy-animi-2"></div>
						<div class="icon-rainy-animi-4"></div>
						<div class="icon-rainy-animi-5"></div>
					</div>
				</div>
			';
		}

		# Rainy Sunny ----------
		elseif( $today_icon == '10d' ){
			$weather_icon = '
				<div class="weather-icon">
          <div class="basecloud"></div>
          <div class="icon-basecloud-bg"></div>
          <div class="animi-icons-wrap">
            <div class="icon-rainy-animi"></div>
            <div class="icon-rainy-animi-2"></div>
            <div class="icon-rainy-animi-4"></div>
            <div class="icon-rainy-animi-5"></div>
          </div>
          <div class="icon-sun-animi"></div>
        </div>
			';
		}

		# Rainy Night ----------
		elseif( $today_icon == '10n' ){
			$weather_icon = '
				<div class="weather-icon">
          <div class="basecloud"></div>
          <div class="icon-basecloud-bg"></div>
          <div class="animi-icons-wrap">
            <div class="icon-rainy-animi"></div>
            <div class="icon-rainy-animi-2"></div>
            <div class="icon-rainy-animi-4"></div>
            <div class="icon-rainy-animi-5"></div>
          </div>
          <div class="icon-moon-animi"></div>
        </div>
			';
		}

		# Thunder ----------
		elseif( $today_icon == '11d' || $today_icon == '11n'){
			$weather_icon = '
				<div class="weather-icon">
          <div class="basecloud"></div>
          <div class="animi-icons-wrap">
            <div class="icon-thunder-animi"></div>
          </div>
        </div>
			';
		}

		# Snowing ----------
		elseif( $today_icon == '13d' || $today_icon == '13n' ){
			$weather_icon = '
				<div class="weather-icon weather-snowing">
          <div class="basecloud"></div>
          <div class="animi-icons-wrap">
            <div class="icon-windysnow-animi"></div>
            <div class="icon-windysnow-animi-2"></div>
          </div>
        </div>
			';
		}

		# Mist ----------
		elseif( $today_icon == '50d'  || $today_icon == '50n' ){
			$weather_icon = '
				<div class="weather-icon">
          <div class="icon-mist"></div>
          <div class="icon-mist-animi"></div>
        </div>
			';
		}


		// Debug Icons ----
		// $weather_icon = '<img src="http://openweathermap.org/img/w/'. $today_icon .'.png">' .$weather_icon;

		return $weather_icon;
	}


	# RETURN ERROR
	function tie_weather_get_error( $msg = false ){

		if( empty( $msg )){
			$msg = esc_html__('No weather information available', 'jannah');
		}

		return '<div class="weather-widget-error">'. $msg .'</div>';

		//return apply_filters( 'tie_weather_get_error', "<!-- TIE WEATHER ERROR: " . $msg . " -->" );
	}

}





/*-----------------------------------------------------------------------------------*/
# Clear the Cached data for specfic City
/*-----------------------------------------------------------------------------------*/
function tie_clear_cached_weather( $location = false ){

	if( ! $location ) return;

	$location = is_numeric( $location ) ? $location : sanitize_title( $location );

	global $wpdb;
	$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->options} WHERE option_name LIKE %s", '_transient_tie_weather_'. $location .'%' ));

}





/*-----------------------------------------------------------------------------------*/
# Update the Cached Weather data after saving the settings
/*-----------------------------------------------------------------------------------*/
add_action( 'tie_options_before_update', 'tie_clear_header_weather' );
function tie_clear_header_weather( $options ){

	$positions = array( 'top-nav', 'main-nav' );

	foreach ( $positions as $pos ){

		if(
			! empty( $options[ $pos.'-components_weather' ] )  &&
			! empty( $options[ $pos.'-components_wz_location' ] ) &&
			! empty( $options[ $pos.'-components_wz_api_key' ] )
		){

			$location = $options[ $pos.'-components_wz_location' ];

			tie_clear_cached_weather( $location );
		}
	}

}

