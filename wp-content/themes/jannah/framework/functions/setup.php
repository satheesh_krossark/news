<?php
/**
 * Theme's Scripts and Styles
 *
 * @package Jannah
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly





/*-----------------------------------------------------------------------------------*/
# Setup Theme
/*-----------------------------------------------------------------------------------*/
add_action( 'after_setup_theme', 'jannah_theme_setup' );
function jannah_theme_setup(){

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-slider' );
	add_theme_support( 'Arqam_Lite' );


	# Post editor styles ----------
	if( ! jannah_get_option( 'disable_editor_styles' ) ){
		add_editor_style( 'css/editor-style.css' );
	}


	# Post Thumbnails ----------
	add_image_size( 'jannah-image-small', 220, 150, true );
	add_image_size( 'jannah-image-large', 390, 220, true );
	add_image_size( 'jannah-image-post',  780, 405, true );
	add_image_size( 'jannah-image-grid',  780, 500, true );
	add_image_size( 'jannah-image-full',  1170, 610,true );


	# Languages ----------
	load_theme_textdomain( 'jannah', JANNAH_TEMPLATE_PATH . '/languages' );


	# Theme Menus ----------
	register_nav_menus( array(
		'top-menu'    => esc_html__( 'Secondry Nav Menu', 'jannah' ),
		'primary'     => esc_html__( 'Main Nav Menu',     'jannah' ),
		'404-menu'    => esc_html__( '404 Page menu',     'jannah' ),
		'footer-menu' => esc_html__( 'Footer Navigation', 'jannah' ),
	));


	# Disable the default bbpress breadcrumb ----------
	add_filter( 'bbp_no_breadcrumb', '__return_true' );

}





/*-----------------------------------------------------------------------------------*/
# Register main Scripts and Styles
/*-----------------------------------------------------------------------------------*/
add_action( 'wp_enqueue_scripts', 'jannah_enqueue_scripts', 20 );
function jannah_enqueue_scripts(){

	$min = jannah_get_option( 'minified_files' ) ? '.min' : '';

	# Scripts files ----------
	// Main Scripts file
	wp_enqueue_script( 'jannah-scripts', JANNAH_TEMPLATE_URL . '/js/scripts'. $min .'.js', array( 'jquery' ), JANNAH_DB_VERSION, true );

	// Sliders
	wp_register_script( 'jannah-sliders', JANNAH_TEMPLATE_URL . '/js/sliders'. $min .'.js', array( 'jquery' ), JANNAH_DB_VERSION, true );

	// Parallax
	wp_register_script( 'jannah-parallax', JANNAH_TEMPLATE_URL . '/js/parallax.js', array( 'jquery', 'imagesloaded' ), JANNAH_DB_VERSION, true );


	# CSS Files ----------
	// Main style.css file
	wp_enqueue_style( 'jannah-styles',  JANNAH_TEMPLATE_URL.'/css/style'. $min .'.css', array(), JANNAH_DB_VERSION, 'all' );

	// WooCommerce
	if ( JANNAH_WOOCOMMERCE_IS_ACTIVE ){
		wp_enqueue_style( 'jannah-woocommerce', JANNAH_TEMPLATE_URL.'/css/woocommerce'. $min .'.css', array(), JANNAH_DB_VERSION, 'all' );
	}

	// bbPress css file
	if ( JANNAH_BBPRESS_IS_ACTIVE ){
		wp_enqueue_style( 'jannah-bbpress', JANNAH_TEMPLATE_URL.'/css/bbpress'. $min .'.css', array(), JANNAH_DB_VERSION, 'all' );

		wp_dequeue_style( 'bbp-default' );
		wp_dequeue_style( 'bbp-default-rtl' );
	}

	// Mp-Timetable css file
	if ( JANNAH_MPTIMETABLE_IS_ACTIVE ){
		wp_enqueue_style( 'jannah-mptt', JANNAH_TEMPLATE_URL.'/css/mptt'. $min .'.css', array(), JANNAH_DB_VERSION, 'all' );
	}

	// Crypto css file
	if ( JANNAH_CRYPTOALL_IS_ACTIVE || JANNAH_WPUC_IS_ACTIVE ){
		wp_enqueue_style( 'jannah-crypto', JANNAH_TEMPLATE_URL.'/css/crypto'. $min .'.css', array(), JANNAH_DB_VERSION, 'all' );
	}

	// iLightBox css file
	$lightbox_skin = jannah_get_option( 'lightbox_skin', 'dark' );
	wp_enqueue_style( 'jannah-ilightbox-skin', JANNAH_TEMPLATE_URL . '/css/ilightbox/'.$lightbox_skin.'-skin/skin.css', array(), JANNAH_DB_VERSION, 'all' );


	# IE ----------
	global $is_IE;

	if( $is_IE ) {

		preg_match( '/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches );
		if( count( $matches ) < 2 ){
  		preg_match( '/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches );
		}

		$version = $matches[1];

		// IE 10 && IE 11
		if( $version <= 11 ){
			wp_enqueue_style( 'jannah-ie-11-styles', JANNAH_TEMPLATE_URL .'/css/ie-lte-11.css', array(), JANNAH_DB_VERSION, 'all' );
			wp_enqueue_script( 'jannah-ie-scripts',   JANNAH_TEMPLATE_URL . '/js/ie.js', array( 'jquery' ), JANNAH_DB_VERSION, true );
		}

		// IE 10
		if ( $version == 10 ) {
			wp_enqueue_style( 'jannah-ie-10-only-styles', JANNAH_TEMPLATE_URL.'/css/ie-10.css', array(), JANNAH_DB_VERSION, 'all' );
		}

		// <= IE 10
		if ( $version < 10 ) {
			wp_enqueue_style( 'jannah-ie-10-styles', JANNAH_TEMPLATE_URL.'/css/ie-lt-10.css', array(), JANNAH_DB_VERSION, 'all' );
		}
	}


	# Queue Comments reply js ----------
	if ( is_singular() && comments_open() && get_option('thread_comments') ){
		wp_enqueue_script( 'comment-reply' );
	}


	# Inline Vars ----------
	$type_to_search = false;
	if( ( jannah_get_option( 'top_nav' )  && jannah_get_option( 'top-nav-components_search'  ) && jannah_get_option( 'top-nav-components_type_to_search'  )) || ( jannah_get_option( 'main_nav' ) && jannah_get_option( 'main-nav-components_search' ) && jannah_get_option( 'main-nav-components_type_to_search' )) ){
		$type_to_search = true;
  }


  # Reading Position Indicator ----------
  if( is_singular() && jannah_get_option( 'reading_indicator' ) ){
  	wp_enqueue_script( 'imagesloaded' );
  }

	$js_vars = array(
		'is_rtl'                => is_rtl(),
		'ajaxurl'               => esc_url(admin_url( 'admin-ajax.php' )),
		'mobile_menu_active'    => jannah_get_option( 'mobile_menu_active' ),
		'mobile_menu_top'       => jannah_get_option( 'mobile_menu_top' ),
		'mobile_menu_parent'    => jannah_get_option( 'mobile_menu_parent_link' ),
		'lightbox_all'          => jannah_get_option( 'lightbox_all' ),
		'lightbox_gallery'      => jannah_get_option( 'lightbox_gallery' ),
		'lightbox_skin'         => $lightbox_skin,
		'lightbox_thumb'        => jannah_get_option( 'lightbox_thumbs' ),
		'lightbox_arrows'       => jannah_get_option( 'lightbox_arrows' ),
		'is_singular'           => is_singular(),
		'is_sticky_video'       => ( is_single() && jannah_get_option( 'sticky_featured_video' ) ),
		'reading_indicator'     => jannah_get_option( 'reading_indicator' ),
		'lazyload'              => jannah_get_option( 'lazy_load' ),
		'select_share'          => jannah_get_option( 'select_share' ),
		'select_share_twitter'  => jannah_get_option( 'select_share_twitter' ),
		'select_share_facebook' => jannah_get_option( 'select_share_facebook' ),
		'select_share_linkedin' => jannah_get_option( 'select_share_linkedin' ),
		'select_share_email'    => jannah_get_option( 'select_share_email' ),
		'facebook_app_id'       => jannah_get_option( 'facebook_app_id' ),
		'twitter_username'      => jannah_get_option( 'share_twitter_username' ),
		'responsive_tables'     => jannah_get_option( 'responsive_tables' ),
		'ad_blocker_detector'   => jannah_get_option( 'ad_blocker_detector' ),

		'sticky_behavior'       => jannah_get_option( 'sticky_behavior' ),
		'sticky_desktop'        => jannah_get_option( 'stick_nav' ),
		'sticky_mobile'         => jannah_get_option( 'stick_mobile_nav' ),

		'is_buddypress_active'  => JANNAH_BUDDYPRESS_IS_ACTIVE,
		'ajax_loader'           => jannah_get_ajax_loader( false ),
		'type_to_search'        => $type_to_search,
		'lang_no_results'       => _ti( 'Nothing Found' ),
	);
	wp_localize_script( 'jquery', 'tie', $js_vars );


	# Taqyeem ----------
	if( JANNAH_TAQYEEM_IS_ACTIVE ){
		wp_dequeue_script( 'taqyeem-main' );

		wp_dequeue_style( 'taqyeem-style' );
		wp_enqueue_style( 'taqyeem-styles', JANNAH_TEMPLATE_URL.'/css/taqyeem'. $min .'.css', array(), JANNAH_DB_VERSION, 'all' );

		if( ! is_admin() ){
			wp_dequeue_style( 'taqyeem-fontawesome' );
		}
	}

	# InstaNOW ----------
	wp_dequeue_style( 'tie-insta-ilightbox-skin' );

	# Prevent InstaNOW Plugin from loading IlightBox ----------
	add_filter( 'tie_instagram_force_avoid_ilightbox', '__return_false' );

	# Prevent TieLabs shortcodes plugin from loading its js and Css files ----------
	add_filter( 'tie_plugin_shortcodes_enqueue_assets', '__return_false' );

	# Remove Query Strings From Static Resources ----------
	if ( ! is_admin() && ! is_user_logged_in() ){
		add_filter( 'script_loader_src', 'jannah_remove_query_strings_1', 15, 1 );
		add_filter( 'style_loader_src',  'jannah_remove_query_strings_1', 15, 1 );
		add_filter( 'script_loader_src', 'jannah_remove_query_strings_2', 15, 1 );
		add_filter( 'style_loader_src',  'jannah_remove_query_strings_2', 15, 1 );
	}

}





/*-----------------------------------------------------------------------------------*/
# Load the Sliders.js file in the Post Slideshow shortcode
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_load_sliders_for_post_slideshow' ) ){

	add_action( 'jannah_extensions_sc_before_post_slideshow', 'jannah_load_sliders_for_post_slideshow' );
	function jannah_load_sliders_for_post_slideshow(){

		# Enqueue the Sliders Js file ----------
		wp_enqueue_script( 'jannah-sliders' );
	}

}





/*-----------------------------------------------------------------------------------*/
# Meta Tags
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_site_head' ) ){

	add_action( 'wp_head', 'jannah_site_head', 5 );
	function jannah_site_head(){

		# Viewport meta tag ----------
		if( jannah_get_option( 'disable_responsive' )){
			echo '<meta name="viewport" content="width=1200" />';
		}
		else{
			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0" />';
		}

		# Theme-color in Chrome 39 for Android ----------
		$theme_color = jannah_get_object_option( 'global_color', 'cat_color', 'post_color' ) ? jannah_get_object_option( 'global_color', 'cat_color', 'post_color' ) : '#0088ff';
		echo "<meta name=\"theme-color\" content=\"$theme_color\" />";

		# Custom Header Code ----------
		echo jannah_get_option('header_code'), "\n";
	}

}





/*-----------------------------------------------------------------------------------*/
# Default fonts sections
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_fonts_sections' )){

	function jannah_fonts_sections(){

		$fonts_sections = array(
			'body'         => 'body',
			'headings'     => '.logo-text, h1, h2, h3, h4, h5, h6',
			'menu'         => '#main-nav .main-menu > ul > li > a',
			'blockquote'   => 'blockquote p',
		);

		return apply_filters( 'jannah_fonts_sections_array', $fonts_sections );
	}

}





/*-----------------------------------------------------------------------------------*/
# Default fonts sections
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_typography_get_css' )){

	function jannah_typography_get_css( $option, $section ){

		$text_styles  = '';
		$text_styles .= ! empty( $section['size'] )      ? 'font-size: '. $section['size'] .'px;'         : '';
		$text_styles .= ! empty( $section['weight'] )    ? 'font-weight: '. $section['weight'] .';'       : '';
		$text_styles .= ! empty( $section['transform'] ) ? 'text-transform: '. $section['transform'] .';' : '';

		if( ! empty( $section['line_height'] ) && $option != 'main_nav' && $option != 'top_menu' ){
			$text_styles .= 'line-height: '. $section['line_height'] .';';
		}

		return $text_styles;
	}

}





/*-----------------------------------------------------------------------------------*/
# Styles
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_get_custom_styling' )){

	function jannah_get_custom_styling(){

		# Get the Fonts CSS ----------
		$out = jannah_insert_fonts_css();


		# Custom size, line height, weight, captelization ----------
		$text_sections = array(
			'body'                 => 'body',
			'site_title'           => '#logo.text-logo .logo-text',
			'top_menu'             => '#top-nav .top-menu > ul > li > a',
			'top_menu_sub'         => '#top-nav .top-menu > ul ul li a',
			'main_nav'             => '#main-nav .main-menu > ul > li > a',
			'main_nav_sub'         => '#main-nav .main-menu > ul ul li a',
			'mobile_menu'          => '#mobile-menu li a',
			'breaking_news'        => '.breaking .breaking-title',
			'breaking_news_posts'  => '.ticker-wrapper .ticker-content',
			'buttons'              => '.button, a.button, a.more-link, .entry a.more-link, input[type="submit"]',
			'breadcrumbs'          => '#breadcrumb',
			'post_cat_label'       => '.post-cat',
			'single_post_title'    => '.entry-header h1.entry-title',
			'single_archive_title' => 'h1.page-title',
			'post_entry'           => '#the-post .entry-content',
			'blockquote'           => '#the-post blockquote',
			'boxes_title'          => '#tie-wrapper .mag-box-title h3',
			'copyright'            => '#tie-wrapper .copyright-text',
			'footer_widgets_title' => '#footer .widget-title h4',
			'post_heading_h1'      => '.entry h1',
			'post_heading_h2'      => '.entry h2',
			'post_heading_h3'      => '.entry h3',
			'post_heading_h4'      => '.entry h4',
			'post_heading_h5'      => '.entry h5',
			'post_heading_h6'      => '.entry h6',
			'widgets_title'        => '
					#tie-wrapper .widget-title h4,
					#tie-wrapper #comments-title,
					#tie-wrapper .comment-reply-title,
					#tie-wrapper .woocommerce-tabs .panel h2,
					#tie-wrapper .related.products h2,
					#tie-wrapper #bbpress-forums #new-post > fieldset.bbp-form > legend,
					#tie-wrapper .entry-content .review-box-header',

			/* Blocks Typography Options */
			'post_title_blocks' => '
          #tie-wrapper .media-page-layout .thumb-title,
          #tie-wrapper .mag-box.full-width-img-news-box .posts-items>li .post-title,
          #tie-wrapper .miscellaneous-box .posts-items>li:first-child h3.post-title,
          #tie-wrapper .big-thumb-left-box .posts-items li:first-child .post-title',
      'post_medium_title_blocks' => '
          #tie-wrapper .mag-box.wide-post-box .posts-items>li:nth-child(n) .post-title,
          #tie-wrapper .mag-box.big-post-left-box li:first-child .post-title,
          #tie-wrapper .mag-box.big-post-top-box li:first-child .post-title,
          #tie-wrapper .mag-box.half-box li:first-child .post-title,
          #tie-wrapper .mag-box.big-posts-box .posts-items>li:nth-child(n) .post-title,
          #tie-wrapper .mag-box.mini-posts-box .posts-items>li:nth-child(n) .post-title,
          #tie-wrapper .mag-box.latest-poroducts-box .products .product h2',
      'post_small_title_blocks' => '
          #tie-wrapper .mag-box.big-post-left-box li:not(:first-child) .post-title,
          #tie-wrapper .mag-box.big-post-top-box li:not(:first-child) .post-title,
          #tie-wrapper .mag-box.half-box li:not(:first-child) .post-title,
          #tie-wrapper .mag-box.big-thumb-left-box li:not(:first-child) .post-title,
          #tie-wrapper .mag-box.scrolling-box .slide .post-title,
          #tie-wrapper .mag-box.miscellaneous-box li:not(:first-child) .post-title',

			/* Sliders Typography Options */
      'post_title_sliders' => array(
      		'min-width: 992px' => '
						.full-width .fullwidth-slider-wrapper .thumb-overlay .thumb-content .thumb-title,
					  .full-width .wide-next-prev-slider-wrapper .thumb-overlay .thumb-content .thumb-title,
					  .full-width .wide-slider-with-navfor-wrapper .thumb-overlay .thumb-content .thumb-title,
					  .full-width .boxed-slider-wrapper .thumb-overlay .thumb-title',
					),
      'post_medium_title_sliders' => array(
      		'min-width: 992px' => '
							.has-sidebar .fullwidth-slider-wrapper .thumb-overlay .thumb-content .thumb-title,
						  .has-sidebar .wide-next-prev-slider-wrapper .thumb-overlay .thumb-content .thumb-title,
						  .has-sidebar .wide-slider-with-navfor-wrapper .thumb-overlay .thumb-content .thumb-title,
						  .has-sidebar .boxed-slider-wrapper .thumb-overlay .thumb-title',
					'min-width: 768px' => '
							#tie-wrapper .main-slider.grid-3-slides .slide .grid-item:nth-child(1) .thumb-title,
						  #tie-wrapper .main-slider.grid-5-first-big .slide .grid-item:nth-child(1) .thumb-title,
						  #tie-wrapper .main-slider.grid-5-big-centerd .slide .grid-item:nth-child(1) .thumb-title,
						  #tie-wrapper .main-slider.grid-4-big-first-half-second .slide .grid-item:nth-child(1) .thumb-title,
						  #tie-wrapper .main-slider.grid-2-big .thumb-overlay .thumb-title,
						  #tie-wrapper .wide-slider-three-slids-wrapper .thumb-title',
					),
      'post_small_title_sliders' => array(
      		'min-width: 768px' => '
							#tie-wrapper .boxed-slider-three-slides-wrapper .slide .thumb-title,
						  #tie-wrapper .grid-3-slides .slide .grid-item:nth-child(n+2) .thumb-title,
						  #tie-wrapper .grid-5-first-big .slide .grid-item:nth-child(n+2) .thumb-title,
						  #tie-wrapper .grid-5-big-centerd .slide .grid-item:nth-child(n+2) .thumb-title,
						  #tie-wrapper .grid-4-big-first-half-second .slide .grid-item:nth-child(n+2) .thumb-title,
						  #tie-wrapper .grid-5-in-rows .grid-item:nth-child(n) .thumb-overlay .thumb-title,
						  #tie-wrapper .main-slider.grid-4-slides .thumb-overlay .thumb-title,
						  #tie-wrapper .grid-6-slides .thumb-overlay .thumb-title,
						  #tie-wrapper .boxed-four-taller-slider .slide .thumb-title',
					),
		);

		foreach ( $text_sections as $option => $elements ){

			if( $section = jannah_get_option( 'typography_'.$option )){

				$text_styles = jannah_typography_get_css( $option, $section );

				if( is_array( $elements ) ){
					foreach ( $elements as $media => $sub_elements ){
						$out .= '@media ('. $media .'){'."\n";
						$out .= "\t".$sub_elements.'{'. $text_styles .'}'."\n";
						$out .= '}'."\n";
					}
				}
				else{
					$out .= "\t".$elements.'{'. $text_styles .'}'."\n";

					# Custom Line Height for the menus ----------
					if( ! empty( $section['line_height'] ) ){

						if( $option == 'main_nav' ){
							$out .= "\t".'#main-nav{line-height: '. $section['line_height'] .'em}'."\n";
						}
						elseif( $option == 'top_menu' ){
							$out .= "\t".'#top-nav{line-height: '. $section['line_height'] .'em}'."\n";
						}
					}


				}

			} // Section option
		}



	/* ============================== Theme Colors ============================== */

		/* Main Colors
			 ===================================================*/

		# Theme Color ----------
		if( $color = jannah_get_object_option( 'global_color', 'cat_color', 'post_color' )){
			$out .= jannah_theme_color( $color );
		}


		# Custom Css Codes for posts and cats ----------
		$out .= jannah_get_object_option( false, 'cat_custom_css', 'tie_custom_css' );


		# Background ----------
		$out .= jannah_theme_background();


		# Highlighted Color ----------
		if( $color = jannah_get_option( 'highlighted_color' )){

			$bright = jannah_light_or_dark( $color );

			$out .="
			::-moz-selection{
				background-color: $color;
				color: $bright;
			}

			::selection{
				background-color: $color;
				color: $bright;
			}";
		}

		# Links Color ----------
		if( $color = jannah_get_option( 'links_color' )){
			$out .="
				a{
					color: $color;
				}
			";
		}

		# Links Color Hover ----------
		if( $color = jannah_get_option( 'links_color_hover' )){
			$out .="
				a:hover{
					color: $color;
				}
			";
		}

		# Links hover underline ----------
		if( jannah_get_option( 'underline_links_hover' )){
			$out .='
			#content a:hover{
				text-decoration: underline !important;
			}';
		}

		# Theme Main Borders ----------
		if( $color = jannah_get_option( 'borders_color' )){

				$out .="
				.container-wrapper,
				.mag-box-title,
				.section-title-default,
				.widget-title,
				#comments-title,
				.comment-reply-title,
				.woocommerce-tabs .panel h2,
				.related.products h2:first-child,
				.up-sells > h2,
				.entry .cross-sells > h2,
				.entry .cart_totals > h2,
				#bbpress-forums #new-post > fieldset.bbp-form > legend,
				.review-box-header,
				.mag-box .show-more-button,
				.woocommerce-tabs ul.tabs li a,
				.tabs-wrapper .tabs-menu li a,
				.tabs-widget .tabs-wrapper .tabs-menu,
				.tabs-widget .tabs-wrapper .tabs-menu .flexMenu-popup,
				.social-statistics-widget .white-bg .social-icons-item a,
				textarea, input, select,
				.woocommerce div.product .woocommerce-tabs,
				.woocommerce div.product .woocommerce-tabs ul.tabs,
				.woocommerce #reviews #comments ol.commentlist li .comment-text,
				.woocommerce div.product .related.products,
				.woocommerce div.product .up-sells.products,
				.woocommerce .cart_totals,
				.woocommerce .cross-sells,
				.woocommerce div.product div.summary .woocommerce-product-details__short-description,
				.woocommerce div.product form.cart{
					border-color: $color !important;
				}

				#footer #footer-widgets-container .fullwidth-area .widget_tag_cloud .tagcloud a:not(:hover){
					background: transparent;
					-webkit-box-shadow: inset 0 0 0 3px $color;
					 -moz-box-shadow: inset 0 0 0 3px $color;
					   -o-box-shadow: inset 0 0 0 3px $color;
					      box-shadow: inset 0 0 0 3px $color;
				}
			";
		}



		/* Secondry nav
			 ===================================================*/

		if( $color = jannah_get_option( 'secondry_nav_background' )){
			$darker = jannah_adjust_color_brightness( $color, -30 );
			$bright = jannah_light_or_dark( $color, true );

			$out .="
			  #tie-wrapper #top-nav{
				  border-width: 0;
			  }

				#tie-wrapper #top-nav,
				#tie-wrapper #top-nav .top-menu ul,
				#tie-wrapper #top-nav .comp-sub-menu,
				#tie-wrapper #top-nav .ticker-content,
				#tie-wrapper #top-nav .ticker-swipe,
				.top-nav-boxed #top-nav .topbar-wrapper,
				.top-nav-dark.top-nav-boxed #top-nav .topbar-wrapper,
				.search-in-top-nav.autocomplete-suggestions{
					background-color : $color;
				}

				#tie-wrapper #top-nav *,
				#tie-wrapper #top-nav .components > li,
				#tie-wrapper #top-nav .comp-sub-menu,
				#tie-wrapper #top-nav .comp-sub-menu li{
					border-color: rgba( $bright, 0.1);
				}

				#tie-wrapper #top-nav .comp-sub-menu .button,
				#tie-wrapper #top-nav .comp-sub-menu .button.guest-btn{
					background-color: $darker;
				}

				#tie-wrapper #top-nav .comp-sub-menu .button,
				#tie-wrapper #top-nav .comp-sub-menu .button.guest-btn,
				.search-in-top-nav.autocomplete-suggestions{
					border-color: $darker;
				}

				#top-nav .weather-menu-item .icon-basecloud-bg:after{
					color: $color;
				}
			";
		}



		# Secondry nav links ----------
		if( $color = jannah_get_option( 'topbar_links_color' )){

			$out .="
				#tie-wrapper #top-nav a,
				#tie-wrapper #top-nav .breaking .ticker a,
				#tie-wrapper #top-nav input,
				#tie-wrapper #top-nav .components button#search-submit,
				#tie-wrapper #top-nav .components button#search-submit .fa-spinner,
				#tie-wrapper #top-nav .top-menu li a,
				#tie-wrapper #top-nav .dropdown-social-icons li a span,
				#tie-wrapper #top-nav .components a.button:hover,
				#tie-wrapper #top-nav .components > li > a,
				#tie-wrapper #top-nav .components > li.social-icons-item .social-link:not(:hover) span,
				#tie-wrapper #top-nav .comp-sub-menu .button:hover,
				#tie-wrapper #top-nav .comp-sub-menu .button.guest-btn:hover,
				#tie-body .search-in-top-nav.autocomplete-suggestions a:not(.button){
					color: $color;
				}

				#tie-wrapper #top-nav input::-moz-placeholder{
					color: $color;
				}

				#tie-wrapper #top-nav input:-moz-placeholder{
					color: $color;
				}

				#tie-wrapper #top-nav input:-ms-input-placeholder{
					color: $color;
				}

				#tie-wrapper #top-nav input::-webkit-input-placeholder{
					color: $color;
				}

				#tie-wrapper #top-nav .top-menu .menu li.menu-item-has-children > a:before{
					border-top-color: $color;
				}

				#tie-wrapper #top-nav .top-menu .menu li li.menu-item-has-children > a:before{
					border-top-color: transparent;
					border-left-color: $color;
				}

				.rtl #tie-wrapper #top-nav .top-menu .menu li li.menu-item-has-children > a:before{
					border-left-color: transparent;
					border-right-color: $color;
				}
			";
		}



		# Secondry nav links on hover ----------
		if( $color = jannah_get_option( 'topbar_links_color_hover' )){

			$darker = jannah_adjust_color_brightness( $color, -30 );
			$bright = jannah_light_or_dark( $color );

			$out .="
				#tie-wrapper #top-nav .comp-sub-menu .button:hover,
				#tie-wrapper #top-nav .comp-sub-menu .button.guest-btn:hover,
				#tie-wrapper #top-nav .comp-sub-menu .button.checkout-button,
				.search-in-top-nav.autocomplete-suggestions a.button{
					background-color: $color;
				}

				#tie-wrapper #top-nav a:hover,
				#tie-wrapper #top-nav .top-menu .menu a:hover,
				#tie-wrapper #top-nav .top-menu .menu li:hover > a,
				#tie-wrapper #top-nav .top-menu .menu > li.tie-current-menu > a,
				#tie-wrapper #top-nav .breaking .ticker a:hover,
				#tie-wrapper #top-nav .components > li > a:hover,
				#tie-wrapper #top-nav .components > li:hover > a,
				#tie-wrapper #top-nav .components button#search-submit:hover,
				.search-in-top-nav.autocomplete-suggestions a:not(.button):hover{
					color: $color;
				}

				#tie-wrapper #top-nav .comp-sub-menu .button:hover,
				#tie-wrapper #top-nav .comp-sub-menu .button.guest-btn:hover{
					border-color: $color;
				}

				#tie-wrapper #top-nav .top-menu .menu li.menu-item-has-children:hover > a:before{
					border-top-color: $color;
				}

				#tie-wrapper #top-nav .top-menu .menu li li.menu-item-has-children:hover > a:before{
					border-top-color: transparent;
					border-left-color: $color;
				}

				.rtl #tie-wrapper #top-nav .top-menu .menu li li.menu-item-has-children:hover > a:before{
					border-left-color: transparent;
					border-right-color: $color;
				}

				#tie-wrapper #top-nav .comp-sub-menu .button:hover,
				#tie-wrapper #top-nav .comp-sub-menu .button.guest-btn:hover,
				#tie-wrapper #top-nav .comp-sub-menu .button.checkout-button:hover,
				#tie-wrapper #top-nav .components a.button:hover,
				#tie-wrapper #top-nav .components a.button.guest-btn:hover,
				#tie-wrapper #top-nav .comp-sub-menu a.button.checkout-button,
				.search-in-top-nav.autocomplete-suggestions .widget-post-list a.button{
					color: $bright;
				}

				#tie-wrapper #theme-header #top-nav .comp-sub-menu .button.checkout-button:hover,
				#tie-body .search-in-top-nav.autocomplete-suggestions a.button:hover{
					background-color: $darker;
				}
			";
		}



		# Top-bar text ----------
		if( $color = jannah_get_option( 'topbar_text_color' )){

			$rgb = jannah_get_rgb_color( $color );

			$out .="
				#tie-wrapper #top-nav,
				#tie-wrapper #top-nav .top-menu ul,
				#tie-wrapper #top-nav .comp-sub-menu,
				.search-in-top-nav.autocomplete-suggestions,
				#top-nav .weather-menu-item .tie-weather-widget{
					color: $color;
				}

				.search-in-top-nav.autocomplete-suggestions .post-meta,
				.search-in-top-nav.autocomplete-suggestions .post-meta a:not(:hover){
					color: rgba( $rgb, 0.7);
				}
			";
		}



		# Breaking News label ----------
		if( $color = jannah_get_option( 'breaking_title_bg' )){

			$bright = jannah_light_or_dark( $color );

			$out .="
				#top-nav .breaking-title{
					color: $bright;
				}

				#top-nav .breaking-title:before{
					background-color: $color;
				}

				#top-nav .breaking-news-nav li:hover{
					background-color: $color;
					border-color: $color;
				}
			";
		}


		/* Main nav
			 ===================================================*/

		if( $color = jannah_get_option( 'main_nav_background' )){

			$bright = jannah_light_or_dark( $color, true );
			$darker = jannah_adjust_color_brightness( $color, -30 );
			$rgb    = jannah_get_rgb_color( $color );

			$out .="
				#tie-wrapper #main-nav{
					background-color : $color;
					border-width: 0;
				}

				#tie-wrapper #main-nav.fixed-nav{
					background-color : rgba( $rgb , 0.95);
				}

				#main-nav .main-menu-wrapper,
				#tie-wrapper .main-nav-boxed #main-nav .main-menu-wrapper,
				#tie-wrapper #main-nav .main-menu .menu li > .sub-menu,
				#tie-wrapper #main-nav .main-menu .menu-sub-content,
				#tie-wrapper #main-nav .comp-sub-menu,
				#tie-body .search-in-main-nav.autocomplete-suggestions{
					background-color: $color;
				}

				#main-nav .weather-menu-item .icon-basecloud-bg:after{
					color: $color;
				}

				#tie-wrapper #main-nav .components > li,
				#tie-wrapper #main-nav .comp-sub-menu,
				#tie-wrapper #main-nav .comp-sub-menu li,
				#tie-wrapper #main-nav .main-menu .menu li > .sub-menu > li > a,
				#tie-wrapper #main-nav .main-menu .menu-sub-content > li > a,
				#tie-wrapper #main-nav .main-menu li.mega-link-column > ul > li > a,
				#tie-wrapper #main-nav .main-menu .mega-recent-featured-list a,
				#tie-wrapper #main-nav .main-menu .mega-cat .mega-cat-more-links > li a,
				#tie-wrapper #main-nav .main-menu .cats-horizontal li a,
				#tie-wrapper .main-menu .mega-cat.menu-item-has-children .mega-cat-wrapper{
					border-color: rgba($bright, 0.07);
				}

				#tie-wrapper #main-nav .comp-sub-menu .button,
        #tie-wrapper #main-nav .comp-sub-menu .button.guest-btn,
				.search-in-main-nav.autocomplete-suggestions{
            border-color: $darker;
        }

				#tie-wrapper #main-nav .comp-sub-menu .button,
				#tie-wrapper #main-nav .comp-sub-menu .button.guest-btn{
					background-color: $darker;
				}

				#tie-wrapper #theme-header.main-nav-boxed #main-nav:not(.fixed-nav){
					background-color: transparent;
				}

				.main-nav-boxed.main-nav-light #main-nav .main-menu-wrapper{
			    border-width: 0;
				}

				.main-nav-boxed.main-nav-below.top-nav-below #main-nav .main-menu-wrapper{
			    border-bottom-width: 1px;
				}
			";
		}


		# Main nav links ----------
		if( $color = jannah_get_option( 'main_nav_links_color' )){

			$out .= "
				#tie-wrapper #main-nav .menu li.menu-item-has-children > a:before,
				#tie-wrapper #main-nav .main-menu .mega-menu > a:before{
					border-top-color: $color;
				}

				#tie-wrapper #main-nav .menu li.menu-item-has-children .menu-item-has-children > a:before,
				#tie-wrapper #main-nav .main-menu .mega-menu .menu-item-has-children > a:before{
					border-top-color: transparent;
					border-left-color: $color;
				}

				.rtl #tie-wrapper #main-nav .menu li.menu-item-has-children .menu-item-has-children > a:before,
				.rtl #tie-wrapper #main-nav .main-menu .mega-menu .menu-item-has-children > a:before{
					border-left-color: transparent;
					border-right-color: $color;
				}

				#tie-wrapper #main-nav .menu > li > a,
				#tie-wrapper #main-nav .menu-sub-content a,
				#tie-wrapper #main-nav .comp-sub-menu a:not(:hover),
				#tie-wrapper #main-nav .dropdown-social-icons li a span,
				#tie-wrapper #main-nav .components a.button:hover,
				#tie-wrapper #main-nav .components > li > a,
				#tie-wrapper #main-nav .comp-sub-menu .button:hover,
				.search-in-main-nav.autocomplete-suggestions a:not(.button){
					color: $color;
				}
			";
		}


		# Main nav Borders ----------
		if( jannah_get_option( 'main_nav_border_top_color' ) || jannah_get_option( 'main_nav_border_top_width' ) ||
			  jannah_get_option( 'main_nav_border_bottom_color' ) || jannah_get_option( 'main_nav_border_bottom_width' ) ){

			// Top
			$border_top_color = jannah_get_option( 'main_nav_border_top_color' ) ? 'border-top-color:'. jannah_get_option( 'main_nav_border_top_color' ) .' !important;'   : '';
			$border_top_width = jannah_get_option( 'main_nav_border_top_width' ) ? 'border-top-width:'. jannah_get_option( 'main_nav_border_top_width' ) .'px !important;' : '';

			// Bottom
			$border_bottom_color = jannah_get_option( 'main_nav_border_bottom_color' ) ? 'border-bottom-color:'. jannah_get_option( 'main_nav_border_bottom_color' ) .' !important;'   : '';
			$border_bottom_width = jannah_get_option( 'main_nav_border_bottom_width' ) ? 'border-bottom-width:'. jannah_get_option( 'main_nav_border_bottom_width' ) .'px !important;' : '';

			$out .= "
				#tie-wrapper #theme-header:not(.main-nav-boxed) #main-nav,
				#tie-wrapper .main-nav-boxed .main-menu-wrapper{
					$border_top_color
					$border_top_width
					$border_bottom_color
					$border_bottom_width
					border-right: 0 none;
					border-left : 0 none;
				}
			";

			if( jannah_get_option( 'main_nav_border_bottom_color' ) || jannah_get_option( 'main_nav_border_bottom_width' )){
				$out .= "
					#tie-wrapper .main-nav-boxed #main-nav.fixed-nav{
						box-shadow: none;
					}
				";
			}
		}


		# Main nav links on hover ----------
		if( $color = jannah_get_option( 'main_nav_links_color_hover' )){

			$darker = jannah_adjust_color_brightness( $color, -30 );
			$bright = jannah_light_or_dark( $color );

			$out .= "
				#tie-wrapper #main-nav .comp-sub-menu .button:hover,
				#tie-wrapper #main-nav .main-menu .menu > li.tie-current-menu,
				#tie-wrapper #main-nav .main-menu .menu > li > .sub-menu,
				#tie-wrapper #main-nav .main-menu .menu > li > .menu-sub-content,
				#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a.is-active,
				#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a:hover{
					border-color: $color;
				}

				#tie-wrapper #main-nav .main-menu .menu > li.tie-current-menu > a,
				#tie-wrapper #main-nav .main-menu .menu > li:hover > a,
				#tie-wrapper #main-nav .main-menu .menu > li > a:hover,
				#tie-wrapper #main-nav .main-menu ul li .mega-links-head:after,

				#tie-wrapper #theme-header #main-nav .comp-sub-menu .button:hover,
				#tie-wrapper #main-nav .comp-sub-menu .button.checkout-button,
				#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a.is-active,
				#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a:hover,
				.search-in-main-nav.autocomplete-suggestions a.button,
				#main-nav .spinner > div{
					background-color: $color;
				}

				#tie-wrapper #main-nav .components a:hover,
				#tie-wrapper #main-nav .components > li > a:hover,
				#tie-wrapper #main-nav .components > li:hover > a,
				#tie-wrapper #main-nav .components button#search-submit:hover,
				#tie-wrapper #main-nav .mega-cat-sub-categories.cats-vertical,
				#tie-wrapper #main-nav .cats-vertical li:hover a,
				#tie-wrapper #main-nav .cats-vertical li a.is-active,
				#tie-wrapper #main-nav .cats-vertical li a:hover,
				#tie-wrapper #main-nav .main-menu .mega-menu .post-meta a:hover,
				#tie-wrapper #main-nav .main-menu .menu .mega-cat-sub-categories.cats-vertical li a.is-active,
				#tie-wrapper #main-nav .main-menu .mega-menu .post-box-title a:hover,
				.search-in-main-nav.autocomplete-suggestions a:not(.button):hover,
				#tie-wrapper #main-nav .spinner-circle:after{
					color: $color;
				}

				#tie-wrapper #main-nav .main-menu .menu > li.tie-current-menu > a,
				#tie-wrapper #main-nav .main-menu .menu > li:hover > a,
				#tie-wrapper #main-nav .main-menu .menu > li > a:hover,
				#tie-wrapper #main-nav .components a.button:hover,
				#tie-wrapper #main-nav .comp-sub-menu a.button.checkout-button,
				#tie-wrapper #main-nav .components a.button.guest-btn:hover,
				#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a.is-active,
				#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a:hover,
				.search-in-main-nav.autocomplete-suggestions .widget-post-list a.button{
					color: $bright;
				}

				#tie-wrapper #main-nav .menu > li.tie-current-menu > a:before,
				#tie-wrapper #theme-header #main-nav .menu > li > a:hover:before,
				#tie-wrapper #theme-header #main-nav .menu > li:hover > a:before{
					border-top-color: $bright;
				}

				.search-in-main-nav.autocomplete-suggestions a.button:hover,
				#tie-wrapper #theme-header #main-nav .comp-sub-menu .button.checkout-button:hover{
					background-color: $darker;
				}
			";
		}

		# Main Nav text ----------
		if( $color = jannah_get_option( 'main_nav_text_color' )){

			$rgb   = jannah_get_rgb_color( $color );

			$out .="
				#tie-wrapper #main-nav,
				#tie-wrapper #main-nav input,
				#tie-wrapper #main-nav .components button#search-submit,
				#tie-wrapper #main-nav .components button#search-submit .fa-spinner,
				#tie-wrapper #main-nav .comp-sub-menu,
				.search-in-main-nav.autocomplete-suggestions,
				#main-nav .weather-menu-item .tie-weather-widget{
					color: $color;
				}

				#tie-wrapper #main-nav input::-moz-placeholder{
					color: $color;
				}

				#tie-wrapper #main-nav input:-moz-placeholder{
					color: $color;
				}

				#tie-wrapper #main-nav input:-ms-input-placeholder{
					color: $color;
				}

				#tie-wrapper #main-nav input::-webkit-input-placeholder{
					color: $color;
				}

				#tie-wrapper #main-nav .main-menu .mega-menu .post-meta,
				#tie-wrapper #main-nav .main-menu .mega-menu .post-meta a:not(:hover){
					color: rgba($rgb, 0.6);
				}

				.search-in-main-nav.autocomplete-suggestions .post-meta,
				.search-in-main-nav.autocomplete-suggestions .post-meta a:not(:hover){
						color: rgba($rgb, 0.7);
				}
			";
		}


	/* In Post links
			 ===================================================*/

		if( jannah_get_option( 'post_links_color' )){
			$out .='
			#the-post .entry-content a:not(.shortc-button){
				color: '. jannah_get_option( 'post_links_color' ) .' !important;
			}';
		}

		if( jannah_get_option( 'post_links_color_hover' )){
			$out .='
			#the-post .entry-content a:not(.shortc-button):hover{
				color: '. jannah_get_option( 'post_links_color_hover' ) .' !important;
			}';
		}



	/* Backgrounds
			 ===================================================*/

		$backround_areas = array(
			'header_background'    => '#tie-wrapper #theme-header',
			'main_content_bg'      => '#tie-container #tie-wrapper, .post-layout-8 #content', // in post-layout-8 tie-wrapper will be transparent so, the #content area,
			'footer_background'    => '#footer',
			'copyright_background' => '#site-info',
			'banner_bg'            => '#background-ad-cover',
		);

		foreach ( $backround_areas as $area => $elements ){

			if( jannah_get_option( $area . '_color' ) || jannah_get_option( $area . '_img' )){

				$background_code  = jannah_get_option( $area . '_color' ) ? 'background-color: '. jannah_get_option( $area . '_color' ) .';' : '';
				$background_image = jannah_get_option( $area . '_img' );

				# Background Image ----------
				$background_code .= jannah_get_background_image_css( $background_image );

				if( ! empty( $background_code ) ){

					$out .=
						$elements .'{
							'. $background_code .'
						}
					';

					# Header Mobile Related Colors ----------
					if( $area == 'header_background' ){

						# Text Site Title color ----------
						if( jannah_get_option( $area . '_color' ) ){

							$out .='
								#logo.text-logo a,
								#logo.text-logo a:hover{
									color: '. jannah_light_or_dark( jannah_get_option( $area . '_color' ) ) .';
								}

								@media (max-width: 991px){
									#tie-wrapper #theme-header .logo-container.fixed-nav{
										background-color: rgba('. jannah_get_rgb_color(jannah_get_option( $area . '_color' )) .', 0.95);
									}
								}
							';
						}

						$out .='
							@media (max-width: 991px){
								#tie-wrapper #theme-header .logo-container{
									'. $background_code .'
								}
							}
						';
					} // Header Custom Colors

				}
			}
		}





	/* Footer area
			 ===================================================*/

		if( jannah_get_option( 'footer_margin_top' ) || jannah_get_option( 'footer_padding_bottom' ) ){

			$footer_margin_top     = jannah_get_option( 'footer_margin_top' ) ? 'margin-top: '. jannah_get_option( 'footer_margin_top' ) .'px;' : '';
			$footer_padding_bottom = jannah_get_option( 'footer_padding_bottom' ) ? 'padding-bottom: '. jannah_get_option( 'footer_padding_bottom' ) .'px;' : '';

			$out .="
				#footer{
					$footer_margin_top
					$footer_padding_bottom
				}
			";
		}

		if( jannah_get_option( 'footer_padding_top' )){
			$out .='
				#footer .footer-widget-area:first-child{
					padding-top: '. jannah_get_option( 'footer_padding_top' ) .'px !important;
				}
			';
		}

		if( $color = jannah_get_option( 'footer_background_color' )){

			$rgb    = jannah_get_rgb_color( $color );
			$darker = jannah_adjust_color_brightness( $color, -30 );
			$bright = jannah_light_or_dark( $color, true );

			$out .="
				#footer .posts-list-counter .posts-list-items li:before{
				  border-color: $color;
				}

				#footer .timeline-widget .date:before{
				  border-color: rgba($rgb, 0.8);
				}

				#footer-widgets-container .footer-boxed-widget-area,
				#footer-widgets-container textarea,
				#footer-widgets-container input:not([type=submit]),
				#footer-widgets-container select,
				#footer-widgets-container code,
				#footer-widgets-container kbd,
				#footer-widgets-container pre,
				#footer-widgets-container samp,
				#footer-widgets-container .latest-tweets-slider-widget .latest-tweets-slider .tie-slider-nav li a:not(:hover),
				#footer-widgets-container .show-more-button,
				#footer-widgets-container .latest-tweets-widget .slider-links .tie-slider-nav span,
				#footer .footer-boxed-widget-area{
				  border-color: rgba($bright, 0.1);
				}

				#footer.dark-skin .social-statistics-widget ul.white-bg li.social-icons-item a,
				#footer.dark-skin ul:not(.solid-social-icons) .social-icons-item a:not(:hover),
				#footer.dark-skin .widget_product_tag_cloud a,
				#footer.dark-skin .widget_tag_cloud .tagcloud a,
				#footer.dark-skin .post-tags a,
				#footer.dark-skin .widget_layered_nav_filters a{
					border-color: rgba($bright, 0.1) !important;
				}

				.dark-skin .social-statistics-widget ul.white-bg li.social-icons-item:before{
				  background: rgba($bright, 0.1);
				}

				#footer-widgets-container .widget-title,
				#footer.dark-skin .social-statistics-widget .white-bg .social-icons-item a span.followers span,
				.dark-skin .social-statistics-widget .circle-three-cols .social-icons-item a span{
				  color: rgba($bright, 0.8);
				}

				#footer-widgets-container .timeline-widget ul:before,
				#footer-widgets-container .timeline-widget .date:before,
				#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li a{
				  background-color: $darker;
				}
			";
		}

		if( jannah_get_option( 'footer_title_color' )){
			$out .='
				#footer-widgets-container .widget-title,
				#footer-widgets-container .widget-title a:not(:hover){
					color: '. jannah_get_option( 'footer_title_color' ) .';
				}
			';
		}

		if( $color = jannah_get_option( 'footer_text_color' )){

			$out .="
				#footer-widgets-container,
				#footer-widgets-container textarea,
				#footer-widgets-container input,
				#footer-widgets-container select,
				#footer-widgets-container .widget_categories li a:before,
				#footer-widgets-container .widget_product_categories li a:before,
				#footer-widgets-container .widget_archive li a:before,
				#footer-widgets-container .wp-caption .wp-caption-text,
				#footer-widgets-container .post-meta,
				#footer-widgets-container .timeline-widget ul li .date,
				#footer-widgets-container .subscribe-widget .subscribe-widget-content h3,
				#footer-widgets-container .about-author .social-icons li.social-icons-item a:not(:hover) span{
					color: $color;
				}

				#footer-widgets-container .meta-item,
				#footer-widgets-container .timeline-widget ul li .date{
				  opacity: 0.8;
				}

				#footer-widgets-container input::-moz-placeholder{
				  color: $color;
				}

				#footer-widgets-container input:-moz-placeholder{
				  color: $color;
				}

				#footer-widgets-container input:-ms-input-placeholder{
				  color: $color;
				}

				#footer-widgets-container input::-webkit-input-placeholder{
				  color: $color;
				}
			";
		}

		if( jannah_get_option( 'footer_links_color' )){
			$out .='
				#footer-widgets-container a:not(:hover){
					color: '. jannah_get_option( 'footer_links_color' ) .';
				}
			';
		}

		if( $color = jannah_get_option( 'footer_links_color_hover' )){

			$darker = jannah_adjust_color_brightness( $color, -30 );
			$bright = jannah_light_or_dark( $color );

			$out .="
				#footer-widgets-container a:hover,
				#footer-widgets-container .post-rating .stars-rating-active,
				#footer-widgets-container .latest-tweets-widget .twitter-icon-wrap span{
					color: $color;
				}

				#footer-widgets-container .digital-rating .pie-svg .circle_bar{
					stroke: $color;
				}

				#footer.dark-skin #instagram-link:before,
				#footer.dark-skin #instagram-link:after,
				#footer-widgets-container .widget.buddypress .item-options a.selected,
				#footer-widgets-container .widget.buddypress .item-options a.loading,
				#footer-widgets-container .tie-slider-nav li > span:hover{
					border-color: $color;
				}

				#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li.is-active a,
				#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li a:hover,
				#footer-widgets-container .digital-rating-static strong,
				#footer-widgets-container .timeline-widget li:hover .date:before,
				#footer-widgets-container #wp-calendar #today,
				#footer-widgets-container .basecloud-bg::before,
				#footer-widgets-container .posts-list-counter .posts-list-items li:before,
				#footer-widgets-container .cat-counter span,
				#footer-widgets-container .widget-title:after,
				#footer-widgets-container .button,
				#footer-widgets-container a.button,
				#footer-widgets-container a.more-link,
				#footer-widgets-container .slider-links a.button,
				#footer-widgets-container input[type='submit'],
				#footer-widgets-container .widget.buddypress .item-options a.selected,
				#footer-widgets-container .widget.buddypress .item-options a.loading,
				#footer-widgets-container .tie-slider-nav li > span:hover,
				#footer-widgets-container .fullwidth-area .widget_tag_cloud .tagcloud a:hover{
					background-color: $color;
					color: $bright;
				}

				#footer-widgets-container .widget.buddypress .item-options a.selected,
				#footer-widgets-container .widget.buddypress .item-options a.loading,
				#footer-widgets-container .tie-slider-nav li > span:hover{
					color: $bright !important;
				}

				#footer-widgets-container .button:hover,
				#footer-widgets-container a.button:hover,
				#footer-widgets-container a.more-link:hover,
				#footer-widgets-container input[type='submit']:hover{
					background-color: $darker;
				}
			";
		}


	/* Copyright area
			 ===================================================*/

		if( jannah_get_option( 'copyright_text_color' )){
			$out .='
			#site-info,
			#site-info ul.social-icons li a span{
				color: '. jannah_get_option( 'copyright_text_color' ) .';
			}';
		}

		if( jannah_get_option( 'copyright_links_color' )){
			$out .='
			#site-info a{
				color: '. jannah_get_option( 'copyright_links_color' ) .';
			}';
		}

		if( jannah_get_option( 'copyright_links_color_hover' )){
			$out .='
			#site-info a:hover{
				color: '. jannah_get_option( 'copyright_links_color_hover' ) .';
			}';
		}


		/* Go to Top Button
			 ===================================================*/

		if( jannah_get_option( 'back_top_background_color' )){
			$out .='
			#go-to-top{
				background: '. jannah_get_option( 'back_top_background_color' ) .';
			}';
		}

		if( jannah_get_option( 'back_top_text_color' )){
			$out .='
			#go-to-top{
				color: '. jannah_get_option( 'back_top_text_color' ) .';
			}';
		}


		# Custom Social Networks colors ----------
		for( $i=1 ; $i<=5 ; $i++ ){
			if ( jannah_get_option( "custom_social_title_$i" ) && jannah_get_option( "custom_social_icon_$i" ) && jannah_get_option( "custom_social_url_$i" ) && jannah_get_option( "custom_social_color_$i" )){

				$color = jannah_get_option( "custom_social_color_$i" );
				$title = jannah_get_option( "custom_social_title_$i" );
				$title = sanitize_title( $title );

				$out .="
					.social-icons-item .custom-link-$i-social-icon{
						background: $color !important;
					}

					.social-icons-item .custom-link-$i-social-icon span{
						color: $color;
					}
				";
			}
		}


		# Colored Categories labels ----------
		$cats_options = get_option( 'tie_cats_options' );
		if( ! empty( $cats_options ) && is_array( $cats_options )){
			foreach ( $cats_options as $cat => $options){
				if( ! empty( $options['cat_color'] )){

					$cat_custom_color = $options['cat_color'];
					$bright_color = jannah_light_or_dark( $cat_custom_color);

					$out .='
						.tie-cat-'.$cat.', .tie-cat-item-'.$cat.' > span{
							background-color:'. $cat_custom_color .' !important;
							color:'. $bright_color .' !important;
						}

						.tie-cat-'.$cat.':after{
							border-top-color:'. $cat_custom_color .' !important;
						}
						.tie-cat-'.$cat.':hover{
							background-color:'. jannah_adjust_color_brightness( $cat_custom_color ) .' !important;
						}

						.tie-cat-'.$cat.':hover:after{
							border-top-color:'. jannah_adjust_color_brightness( $cat_custom_color ) .' !important;
						}
					';
				}
			}
		}


		# Arqam Plugin Custom colors ----------
		if( JANNAH_ARQAM_IS_ACTIVE ){
			$arqam_options = get_option( 'arq_options' );
			if( ! empty( $arqam_options['color'] ) && is_array( $arqam_options['color'] )){
				foreach ( $arqam_options['color'] as $social => $color ){
					if( ! empty( $color )){
						if( $social == '500px' ){
							$social = 'px500';
						}
						$out .= "
							.social-statistics-widget .solid-social-icons .social-icons-item .$social-social-icon{
								background-color: $color !important;
								border-color: $color !important;
							}
							.social-statistics-widget .$social-social-icon span.counter-icon{
								background-color: $color !important;
							}
						";
					}
				}
			}
		}

		# Take Over Ad top margin ----------
		if( jannah_get_option( 'banner_bg' ) && jannah_get_option( 'banner_bg_url' ) && jannah_get_option( 'banner_bg_site_margin' ) ){
			$out .= '
				@media (min-width: 992px){
					#tie-wrapper{
						margin-top: '. jannah_get_option( 'banner_bg_site_margin' ) .'px !important;
					}
				}
			';
		}

		# Site Width ----------
		if( jannah_get_option( 'site_width' ) && jannah_get_option( 'site_width' ) != '1200px' ){
			$out .= '
				@media (min-width: 1200px){
				.container{
						width: auto;
					}
				}
			';

			if( strpos( jannah_get_option( 'site_width' ), '%' ) !== false ){
				$out .= '
					@media (min-width: 992px){
						.container{
							max-width: '.jannah_get_option( 'site_width' ).';
						}
						body.boxed-layout #tie-wrapper,
						body.boxed-layout .fixed-nav{
							max-width: '.jannah_get_option( 'site_width' ).';
						}
						body.boxed-layout .container{
							max-width: 100%;
						}
					}
				 ';
			}
			else{
				$outer_width = str_replace( 'px', '', jannah_get_option( 'site_width' ) ) + 30;
				$out .= '
					body.boxed-layout #tie-wrapper,
					body.boxed-layout .fixed-nav{
						max-width: '.  $outer_width .'px;
					}
					@media (min-width: '.jannah_get_option( 'site_width' ).'){
						.container{
							max-width: '.jannah_get_option( 'site_width' ).';
						}
					}
				';
			}
		}


		# Mobile Menu Background ----------
		if( jannah_get_option( 'mobile_menu_active' ) ){

			if( jannah_get_option( 'mobile_menu_background_type' ) == 'color' ){
				if( jannah_get_option( 'mobile_menu_background_color' ) ){
					$mobile_bg = 'background-color: '. jannah_get_option( 'mobile_menu_background_color' ) .';';
					$out .='
						@media (max-width: 991px){
							.side-aside #mobile-menu .menu > li{
								border-color: rgba('.jannah_light_or_dark( jannah_get_option( 'mobile_menu_background_color' ), true ).',0.05);
							}
							.side-aside #mobile-search .search-field{
								background-color: rgba('. jannah_light_or_dark( jannah_get_option( 'mobile_menu_background_color' ), true).',0.05);
							}
						}
					';
				}
			}

			elseif( jannah_get_option( 'mobile_menu_background_type' ) == 'gradient' ){
				if( jannah_get_option( 'mobile_menu_background_gradient_color_1' ) &&  jannah_get_option( 'mobile_menu_background_gradient_color_2' ) ){
					$color1 = jannah_get_option( 'mobile_menu_background_gradient_color_1' );
					$color2 = jannah_get_option( 'mobile_menu_background_gradient_color_2' );

					$mobile_bg = '
						background: '. $color1 .';
						background: -webkit-linear-gradient(135deg, '. $color1 .', '. $color2 .' );
						background:    -moz-linear-gradient(135deg, '. $color1 .', '. $color2 .' );
						background:      -o-linear-gradient(135deg, '. $color1 .', '. $color2 .' );
						background:         linear-gradient(135deg, '. $color1 .', '. $color2 .' );
					';
				}
			}

			elseif ( jannah_get_option( 'mobile_menu_background_type' ) == 'image' ){
				if( jannah_get_option( 'mobile_menu_background_image' ) ){
					$background_image = jannah_get_option( 'mobile_menu_background_image' );
					$mobile_bg = jannah_get_background_image_css( $background_image );
				}
			}


			if( ! empty( $mobile_bg ) ){
				$out .='
					@media (max-width: 991px){
						.side-aside.dark-skin{
							'.$mobile_bg.'
						}
					}
				';
			}

			if( jannah_get_option( 'mobile_menu_icon_color' ) ){
				$out .='
					#mobile-menu-icon .menu-text{
						color: '. jannah_get_option( 'mobile_menu_icon_color' ) .'!important;
					}
					#mobile-menu-icon .nav-icon,
					#mobile-menu-icon .nav-icon:before,
					#mobile-menu-icon .nav-icon:after{
						background-color: '. jannah_get_option( 'mobile_menu_icon_color' ) .'!important;
					}
				';
			}

			if( jannah_get_option( 'mobile_menu_text_color' ) ){
				$out .='
					.side-aside #mobile-menu li a,
					.side-aside #mobile-menu .mobile-arrows,
					.side-aside #mobile-search .search-field{
						color: '. jannah_get_option( 'mobile_menu_text_color' ) .';
					}

					#mobile-search .search-field::-moz-placeholder {
						color: '. jannah_get_option( 'mobile_menu_text_color' ) .';
					}

					#mobile-search .search-field:-moz-placeholder {
						color: '. jannah_get_option( 'mobile_menu_text_color' ) .';
					}

					#mobile-search .search-field:-ms-input-placeholder {
						color: '. jannah_get_option( 'mobile_menu_text_color' ) .';
					}

					#mobile-search .search-field::-webkit-input-placeholder {
						color: '. jannah_get_option( 'mobile_menu_text_color' ) .';
					}

					@media (max-width: 991px){
						.tie-btn-close span{
							color: '. jannah_get_option( 'mobile_menu_text_color' ) .';
						}
					}
				';
			}

			if( jannah_get_option( 'mobile_menu_social_color' ) ){
				$out .='
					#mobile-social-icons .social-icons-item a:not(:hover) span{
						color: '. jannah_get_option( 'mobile_menu_social_color' ) .'!important;
					}
				';
			}

			if( jannah_get_option( 'mobile_menu_search_color' ) ){
				$search_color = jannah_get_option( 'mobile_menu_search_color' );
				$out .='
					#mobile-search .search-submit{
						background-color: '. $search_color .';
						color: '.jannah_light_or_dark( $search_color ).';
					}

					#mobile-search .search-submit:hover{
						background-color: '. jannah_adjust_color_brightness( $search_color ) .';
					}
				';
			}

		}


		# Post Title Poppins ----------
		/*
		$title_poppins = jannah_get_option( 'typography_headings_google_font' );
		if( ! empty( $title_poppins ) && $title_poppins == 'Poppins' && ! is_rtl() ){
			$out .='
				.wf-active .logo-text, .wf-active h1{
	    		letter-spacing: -0.02em;
				}
				.wf-active h2, .wf-active h3, .wf-active h4, .wf-active h5, .wf-active h6{
	    		letter-spacing: -.04em;
				}
			';
		}
		*/


		# Custom CSS codes ----------
		$out .= jannah_get_option( 'css' );
		$out .= jannah_custom_css_media_query( 'css_tablets', 1024, 768 );
		$out .= jannah_custom_css_media_query( 'css_phones', 768, 0 );


		# Prepare the CSS codes ----------
		$out = apply_filters( 'jannah_custom_css', $out );
		$out = jannah_minify_css( $out );

		return $out;
	}

}





/*-----------------------------------------------------------------------------------*/
# Custom Theme Color
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_theme_color' )){

	function jannah_theme_color( $color ){
		$dark_color = jannah_adjust_color_brightness( $color, -50 );
		$rgb_color  = jannah_get_rgb_color( $color );
		$bright     = jannah_light_or_dark( $color );


		/* Color ----------------------------------------*/
			// .brand-title is an extra class used to set the band color for special texts
		$skin = "
			.brand-title,
			a:hover,
			#tie-popup-search-submit,
			.post-rating .stars-rating-active,
			.components button#search-submit:hover,
			#logo.text-logo a,
			#tie-wrapper #top-nav a:hover,
			#tie-wrapper #top-nav .breaking a:hover,
			#tie-wrapper #main-nav .components a:hover,
			#theme-header #top-nav .components > li > a:hover,
			#theme-header #top-nav .components > li:hover > a,
			#theme-header #main-nav .components > li > a:hover,
			#theme-header #main-nav .components > li:hover > a,
			#top-nav .top-menu .menu > li.tie-current-menu > a,
			#tie-wrapper #top-nav .top-menu .menu li:hover > a,
			#tie-wrapper #top-nav .top-menu .menu a:hover,
			#tie-wrapper #main-nav .main-menu .mega-menu .post-box-title a:hover,
			#tie-wrapper #main-nav .main-menu .menu .mega-cat-sub-categories.cats-vertical li:hover a,
			#tie-wrapper #main-nav .main-menu .menu .mega-cat-sub-categories.cats-vertical li a.is-active,
			#tie-wrapper .mag-box .mag-box-options .mag-box-filter-links li > a.active,
			.mag-box .mag-box-options .mag-box-filter-links li:hover > a.active,
			.mag-box .mag-box-options .mag-box-filter-links .flexMenu-viewMore > a:hover,
			.mag-box .mag-box-options .mag-box-filter-links .flexMenu-viewMore:hover > a,
			.box-dark-skin.mag-box .posts-items > li .post-title a:hover,
			.dark-skin .mag-box .post-meta .post-rating .stars-rating-active span.fa,
			.box-dark-skin .post-meta .post-rating .stars-rating-active span.fa,
			#go-to-content:hover,
			.comment-list .comment-author .fn,
			.commentlist .comment-author .fn,
			blockquote::before,
			blockquote cite,
			blockquote.quote-simple p,
			.multiple-post-pages a:hover,
			#story-index li .is-current,
			body:not(.blocks-title-style-8) .mag-box .container-wrapper .mag-box-title h3 a,
			body:not(.blocks-title-style-8) .mag-box .container-wrapper .mag-box-title a.block-more-button,
			.tabs-menu li.active > a,
			.tabs-menu li.is-active a,
			.latest-tweets-widget .twitter-icon-wrap span,
			.wide-next-prev-slider-wrapper .tie-slider-nav li:hover span,
			.video-playlist-nav-wrapper .video-playlist-item .video-play-icon,
			#instagram-link:hover,
			.review-final-score h3,
			#mobile-menu-icon:hover .menu-text,
			.tabs-wrapper .tabs-menu li.active > a,
			.tabs-wrapper .tabs-menu li.is-active a,
			.entry a:not(:hover),
			.entry .post-bottom-meta a[href]:hover,
			#footer-widgets-container a:hover,
			#footer-widgets-container .post-rating .stars-rating-active,
			#footer-widgets-container .latest-tweets-widget .twitter-icon-wrap span,
			#site-info a:hover,
			.spinner-circle:after,
			.widget.tie-weather-widget .icon-basecloud-bg:after{
				color: $color;
			}
		";


		// To fix an overwrite issue ----------
		if( $main_nav_color = jannah_get_option( 'main_nav_links_color_hover' )){
			$skin .="
				#theme-header #main-nav .spinner-circle:after{
					color: $color;
				}
			";
		}


		/* Background-color -----------------------------*/
		$skin .="
			.button,
			a.button,
			a.more-link,
			.entry a.more-link,
			#tie-wrapper #theme-header .comp-sub-menu .button:hover,
			#tie-wrapper #theme-header .comp-sub-menu .button.guest-btn:hover,
			#tie-wrapper #theme-header .comp-sub-menu .button.checkout-button,
			#tie-wrapper #theme-header #main-nav .comp-sub-menu .button:hover,
			.dark-skin a.more-link:not(:hover),
			input[type='submit'],
			.post-cat,
			.tie-slider-nav li > span:hover,
			.pages-nav .next-prev li.current span,
			.pages-nav .pages-numbers li.current span,
			#tie-wrapper .mejs-container .mejs-controls,
			.spinner > div,
			#mobile-menu-icon:hover .nav-icon,
			#mobile-menu-icon:hover .nav-icon:before,
			#mobile-menu-icon:hover .nav-icon:after,
			#theme-header #main-nav .main-menu .menu > li.tie-current-menu > a,
			#theme-header #main-nav .main-menu .menu > li:hover > a,
			#theme-header #main-nav .main-menu .menu > li > a:hover,
			#tie-wrapper #main-nav .main-menu ul li .mega-links-head:after,
			#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a.is-active,
			#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a:hover,
			.main-nav-dark .main-menu .menu > li > a:hover,
			#mobile-menu-icon:hover .nav-icon,
			#mobile-menu-icon:hover .nav-icon:before,
			#mobile-menu-icon:hover .nav-icon:after,
			.mag-box .mag-box-options .mag-box-filter-links li a:hover,
			.slider-arrow-nav a:not(.pagination-disabled):hover,
			.comment-list .reply a:hover,
			.commentlist .reply a:hover,
			#reading-position-indicator,
			.multiple-post-pages > span,
			#story-index-icon,
			.posts-list-counter .posts-list-items li:before,
			.cat-counter span,
			.digital-rating-static strong,
			#wp-calendar #today,
			.basecloud-bg,
			.basecloud-bg::before,
			.basecloud-bg::after,
			.timeline-widget ul li a:hover .date:before,
			.cat-counter a + span,
			.video-playlist-nav-wrapper .playlist-title,
			.review-percentage .review-item span span,
			.slick-dots li.slick-active button,
			.slick-dots li button:hover,
			#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li.is-active a,
			#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li a:hover,
			#footer-widgets-container .digital-rating-static strong,
			#footer-widgets-container .timeline-widget li:hover .date:before,
			#footer-widgets-container #wp-calendar #today,
			#footer-widgets-container .basecloud-bg::before,
			#footer-widgets-container .posts-list-counter .posts-list-items li:before,
			#footer-widgets-container .cat-counter span,
			#footer-widgets-container .widget-title:after,
			#footer-widgets-container .button,
			#footer-widgets-container a.button,
			#footer-widgets-container a.more-link,
			#footer-widgets-container .slider-links a.button,
			#footer-widgets-container input[type='submit'],
			#footer-widgets-container .tie-slider-nav li > span:hover,
			#footer-widgets-container .fullwidth-area .widget_tag_cloud .tagcloud a:hover,
			.wide-slider-nav-wrapper .slide:after,
			.demo_store,
			.demo #logo:after,
			.widget.tie-weather-widget,
			span.video-close-btn:hover,
			#go-to-top{
				background-color: $color;
				color: $bright;
			}
		";


		$skin .="
			.side-aside.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li a:hover,
			.side-aside.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li.is-active a,
			#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li a:hover,
			#footer.dark-skin .tabs-widget .tabs-wrapper .tabs-menu li.is-active a{
				background-color: $color !important;
				color: $bright;
			}
		";


		/* border-color ---------------------------------*/
		$skin .="
			pre,
			code,
			.pages-nav .next-prev li.current span,
			.pages-nav .pages-numbers li.current span,
			#tie-wrapper #theme-header .comp-sub-menu .button:hover,
			#tie-wrapper #theme-header .comp-sub-menu .button.guest-btn:hover,
			.multiple-post-pages > span,
			.post-content-slideshow .tie-slider-nav li span:hover,
			.latest-tweets-widget .slider-links .tie-slider-nav li span:hover,
			.dark-skin .latest-tweets-widget .slider-links .tie-slider-nav span:hover,
			#instagram-link:before,
			#instagram-link:after,
			.mag-box .mag-box-options .mag-box-filter-links li a:hover,
			.mag-box .mag-box-options .slider-arrow-nav a:not(.pagination-disabled):hover,
			#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a.is-active,
			#theme-header #main-nav .menu .mega-cat-sub-categories.cats-horizontal li a:hover,
			#footer.dark-skin #instagram-link:before,
			#footer.dark-skin #instagram-link:after,
			#footer-widgets-container .tie-slider-nav li > span:hover,
			#theme-header #main-nav .main-menu .menu > li > .sub-menu,
			#theme-header #main-nav .main-menu .menu > li > .menu-sub-content{
				border-color: $color;
			}

			#tie-wrapper #top-nav .top-menu .menu li.menu-item-has-children:hover > a:before{
				border-top-color: $color;
			}

			#theme-header .main-menu .menu > li.tie-current-menu > a:before,
			#theme-header #main-nav .main-menu .menu > li > a:hover:before,
			#theme-header #main-nav .main-menu .menu > li:hover > a:before{
				border-top-color: $bright;
			}

			#tie-wrapper #top-nav .top-menu .menu li li.menu-item-has-children:hover > a:before{
				border-left-color: $color;
				border-top-color: transparent;
			}

			.rtl #tie-wrapper #top-nav .top-menu .menu li li.menu-item-has-children:hover > a:before{
				border-right-color: $color;
				border-top-color: transparent;
			}

			#tie-wrapper #main-nav .main-menu .menu > li.tie-current-menu{
				border-bottom-color: $color;
			}
		";

		/* Footer Border Top ---------------------------------*/
		if( jannah_get_option( 'footer_border_top' )){
			$skin .="
				#footer-widgets-container{
					border-top: 8px solid $color;
					-webkit-box-shadow: 0 -5px 0 rgba(0,0,0,0.07);
					   -moz-box-shadow: 0 -8px 0 rgba(0,0,0,0.07);
					        box-shadow: 0 -8px 0 rgba(0,0,0,0.07);
				}
			";
		}


		/* Misc ----------------------------------------------*/
		$skin .="
			::-moz-selection{
				background-color: $color;
				color: $bright;
			}

			::selection{
				background-color: $color;
				color: $bright;
			}

			.digital-rating .pie-svg .circle_bar,
			#footer-widgets-container .digital-rating .pie-svg .circle_bar{
				stroke: $color;
			}

			#reading-position-indicator{
				box-shadow: 0 0 10px rgba( $rgb_color, 0.7);
			}
		";


		/* Dark Color ----------------------------------------*/
		$skin .="
			#tie-popup-search-submit:hover,
			#logo.text-logo a:hover,
			.entry a:hover,
			body:not(.blocks-title-style-8) .mag-box .container-wrapper .mag-box-title h3 a:hover,
			body:not(.blocks-title-style-8) .mag-box .container-wrapper .mag-box-title a.block-more-button:hover{
				color: $dark_color;
			}
		";


		/* Dark Background-color -----------------------------*/
		$skin .="
			.button:hover,
			a.button:hover,
			a.more-link:hover,
			.entry a.more-link:hover,
			input[type='submit']:hover,
			a.post-cat:hover,
			#footer-widgets-container .button:hover,
			#footer-widgets-container a.button:hover,
			#footer-widgets-container a.more-link:hover,
			#footer-widgets-container input[type='submit']:hover{
				background-color: $dark_color;
			}

			.search-in-main-nav.autocomplete-suggestions a.button:hover,
			#tie-wrapper #theme-header #top-nav .comp-sub-menu .button.checkout-button:hover,
			#tie-wrapper #theme-header #main-nav .comp-sub-menu .button.checkout-button:hover{
				background-color: $dark_color;
				color: $bright;
			}

			#theme-header #main-nav .comp-sub-menu a.checkout-button:not(:hover),
			#theme-header #top-nav .comp-sub-menu a.checkout-button:not(:hover),
			.entry a.button{
				color: $bright;
			}

			#footer-widgets-container .tie-slider-nav li > span:hover{
				color: $bright !important;
			}

			@media (max-width: 1600px){
				#story-index ul{ background-color: $color; }
				#story-index ul li a, #story-index ul li .is-current{ color: $bright; }
			}
		";


		/* BuddyPress ----------------------------------------*/
		if ( JANNAH_BUDDYPRESS_IS_ACTIVE ){
			$skin .="
				#buddypress .activity-list li.load-more a:hover,
				#buddypress .activity-list li.load-newest a:hover,
				#buddypress #item-header #item-meta #latest-update a,
				#buddypress .item-list-tabs ul li a:hover,
				#buddypress .item-list-tabs ul li.selected a,
				#buddypress .item-list-tabs ul li.current a,
				#buddypress .item-list-tabs#subnav ul li a:hover,
				#buddypress .item-list-tabs#subnav ul li.selected a,
				#buddypress a.unfav:after,
				#buddypress a.message-action-unstar:after,
				#buddypress .profile .profile-fields .label{
					color: $color;
				}

				#buddypress .activity-meta a.button:hover,
				#buddypress table.sitewide-notices tr td:last-child a:hover,
				#buddypress table.sitewide-notices tr.alt td:last-child a:hover
				#profile-edit-form ul.button-nav li a:hover,
				#profile-edit-form ul.button-nav li.current a{
					color: $color !important;
				}

				#buddypress input[type=submit],
				#buddypress input[type=button],
				#buddypress button[type=submit],
				#buddypress a.button,
				#buddypress a#bp-delete-cover-image,
				#buddypress input[type=submit]:focus,
				#buddypress input[type=button]:focus,
				#buddypress button[type=submit]:focus,
				#buddypress .item-list-tabs ul li a span,
				#buddypress .profile .profile-fields .label:before,
				.widget.buddypress .item-options a.selected,
				.widget.buddypress .item-options a.loading,
				#footer-widgets-container .widget.buddypress .item-options a.selected,
				#footer-widgets-container .widget.buddypress .item-options a.loading{
					background-color: $color;
					color: $bright;
				}

				#buddypress .activity-meta a.button:hover,
				#buddypress .item-list-tabs#subnav ul li.selected a,
				.widget.buddypress .item-options a.selected,
				.widget.buddypress .item-options a.loading,
				#footer-widgets-container .widget.buddypress .item-options a.selected,
				#footer-widgets-container .widget.buddypress .item-options a.loading{
					border-color: $color;
				}

				#buddypress #whats-new:focus{
					border-color: $color !important;
				}

				#buddypress input[type=submit]:hover,
				#buddypress input[type=button]:hover,
				#buddypress button[type=submit]:hover,
				#buddypress a.button:hover,
				#buddypress a#bp-delete-cover-image:hover{
					background-color: $dark_color;
				}

				#footer-widgets-container .widget.buddypress .item-options a.selected,
				#footer-widgets-container .widget.buddypress .item-options a.loading{
					color: $bright !important;
				}

				@-webkit-keyframes loader-pulsate {
					from {
						box-shadow: 0 0 0 4px rgba($rgb_color, 0.2);
					}
					to {
						box-shadow: 0 0 0 0 rgba($rgb_color, 0.2);
					}
				}

				@keyframes loader-pulsate {
					from {
						box-shadow: 0 0 0 4px rgba($rgb_color, 0.2);
					}
					to {
						box-shadow: 0 0 0 0 rgba($rgb_color, 0.2);
					}
				}
			";
		}


		/* WooCommerce ----------------------------------------*/
		if ( JANNAH_WOOCOMMERCE_IS_ACTIVE ){
			$skin .="
				.woocommerce-tabs ul.tabs li.active a,
				.woocommerce-tabs ul.tabs li.is-active a,
				.woocommerce div.product span.price,
				.woocommerce div.product p.price,
				.woocommerce div.product div.summary .product_meta > span,
				.woocommerce div.product div.summary .product_meta > span a:hover,
				.woocommerce ul.products li.product .price ins,
				.woocommerce .woocommerce-pagination .page-numbers li a.current,
				.woocommerce .woocommerce-pagination .page-numbers li a:hover,
				.woocommerce .woocommerce-pagination .page-numbers li span.current,
				.woocommerce .woocommerce-pagination .page-numbers li span:hover,
				.woocommerce .widget_rating_filter ul li.chosen a,
				.woocommerce-MyAccount-navigation ul li.is-active a{
					color: $color;
				}

				.woocommerce span.new,
				.woocommerce a.button.alt,
				.woocommerce button.button.alt,
				.woocommerce input.button.alt,
				.woocommerce a.button.alt.disabled,
				.woocommerce a.button.alt:disabled,
				.woocommerce a.button.alt:disabled[disabled],
				.woocommerce a.button.alt.disabled:hover,
				.woocommerce a.button.alt:disabled:hover,
				.woocommerce a.button.alt:disabled[disabled]:hover,
				.woocommerce button.button.alt.disabled,
				.woocommerce button.button.alt:disabled,
				.woocommerce button.button.alt:disabled[disabled],
				.woocommerce button.button.alt.disabled:hover,
				.woocommerce button.button.alt:disabled:hover,
				.woocommerce button.button.alt:disabled[disabled]:hover,
				.woocommerce input.button.alt.disabled,
				.woocommerce input.button.alt:disabled,
				.woocommerce input.button.alt:disabled[disabled],
				.woocommerce input.button.alt.disabled:hover,
				.woocommerce input.button.alt:disabled:hover,
				.woocommerce input.button.alt:disabled[disabled]:hover,
				.woocommerce .widget_price_filter .ui-slider .ui-slider-range{
					background-color: $color;
					color: $bright;
				}

				.woocommerce div.product #product-images-slider-nav .tie-slick-slider .slide.slick-current img{
					border-color: $color;
				}

				.woocommerce a.button:hover,
				.woocommerce button.button:hover,
				.woocommerce input.button:hover,
				.woocommerce a.button.alt:hover,
				.woocommerce button.button.alt:hover,
				.woocommerce input.button.alt:hover{
					background-color: $dark_color;
				}
			";

		}

		$skin .= jannah_blocks_head( $color );


		return $skin;
	}

}





/*-----------------------------------------------------------------------------------*/
# Custom Theme Color
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_blocks_head' )){

	function jannah_blocks_head( $color ){

		$out        = '';
		$dark_color = jannah_adjust_color_brightness( $color, 10 );
		$bright     = jannah_light_or_dark( $color );
		$rgb_bright_color = jannah_get_rgb_color( $bright );

		$block_style = jannah_get_option( 'blocks_style', 1 );

		$out .="
			#tie-body a.block-more-button:hover{
				color: $dark_color;
			}
		";

		/* Style #1 */
		if( $block_style == 1 ){

			$out .= "
				#tie-body .mag-box .mag-box-title{
					color: $color;
				}

				#tie-body .mag-box .mag-box-title:before {
					border-top-color: $color;
				}

				#tie-body .mag-box .mag-box-title:after,
				#tie-body #footer .widget-title:after{
					background-color: $color;
				}
			";
		}

		/* Style #2 */
		elseif( $block_style == 2 ){

			$out .= "
				#tie-body .section-title-default,
				#tie-body .mag-box .mag-box-title,
				#tie-body #comments-title,
				#tie-body .comment-reply-title,
				#tie-body .woocommerce-tabs .panel h2,
				#tie-body .related.products h2:first-child,
				#tie-body .up-sells>h2,
				#tie-body .entry .cross-sells>h2,
				#tie-body .entry .cart_totals>h2,
				#tie-body #bbpress-forums #new-post>fieldset.bbp-form>legend,
				#tie-body .review-box-header{
					border-color: $color;
					color: $color;
				}

				#tie-body #footer .widget-title h4:after{
					background-color: $color;
				}
			";
		}

		/* Style #3 */
		elseif( $block_style == 3 ){

			$out .= "
				#tie-body .mag-box .mag-box-title{
					color: $color;
				}

				#tie-body .mag-box .mag-box-title:after {
					background: $color;
				}

				#tie-body #footer .widget-title:after{
					background-color: $color;
				}
			";
		}

		/* Style #4 || #5 || #6 */
		elseif( $block_style == 4 || $block_style == 5 || $block_style == 6 ){

			$out .= "
				#tie-body .section-title-default,
				#tie-body .mag-box-title h3,
				#tie-body .mag-box-title h3 a,
				#tie-body #comments-title,
				#tie-body .comment-reply-title,
				#tie-body .review-box-header,
				#tie-body .woocommerce-tabs .panel h2,
				#tie-body .related.products h2:first-child,
				#tie-body .up-sells>h2,
				#tie-body .entry .cross-sells>h2,
				#tie-body .entry .cart_totals>h2,
				#tie-body #bbpress-forums #new-post>fieldset.bbp-form>legend{
					color: $bright;
				}

				#tie-body .section-title-default:before,
				#tie-body .mag-box-title h3:before,
				#tie-body #comments-title:before,
				#tie-body .comment-reply-title:before,
				#tie-body .review-box-header:before,
				#tie-body .woocommerce-tabs .panel h2:before,
				#tie-body .related.products h2:first-child:before,
				#tie-body .up-sells>h2:before,
				#tie-body .entry .cross-sells>h2:before,
				#tie-body .entry .cart_totals>h2:before,
				#tie-body #bbpress-forums #new-post>fieldset.bbp-form>legend:before{
					background-color: $color;
				}
			";

			if( $block_style == 5 ){

				$out .= "
					#tie-body .woocommerce-tabs ul.tabs>li.active a:before,
					#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:before,
					#tie-body .woocommerce-tabs ul.tabs>li.active:first-child a:after,
					#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li.is-active:first-child a:after,
					#tie-body .widget .tabs-widget .tabs-wrapper .tabs-menu li.is-active:last-child a:after {
						background-color: $color;
					}
				";
			}
			elseif( $block_style == 6 ){

				$out .= "
					#tie-body .section-title-default:after,
					#tie-body .mag-box-title h3:after,
					#tie-body #comments-title:after,
					#tie-body .comment-reply-title:after,
					#tie-body .review-box-header:after,
					#tie-body .woocommerce-tabs .panel h2:after,
					#tie-body .related.products h2:first-child:after,
					#tie-body .up-sells>h2:after,
					#tie-body .entry .cross-sells>h2:after,
					#tie-body .entry .cart_totals>h2:after,
					#tie-body #bbpress-forums #new-post>fieldset.bbp-form>legend:after{
						background-color: $color;
					}
				";
			}

			// Magazine 2 ----------
			if( jannah_get_option( 'boxes_style' ) == 2 ){

				$out .= "
					#tie-body .woocommerce-tabs ul.tabs,
					#tie-body .tabs-widget .tabs-wrapper .tabs-menu{
						border-color: $color !important;
					}

					#tie-body .woocommerce-tabs ul.tabs > li > a,
					#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li>a,
					#tie-body .tabs-menu .flexMenu-popup li a {
						color: $color;
					}

					#tie-body .woocommerce-tabs ul.tabs>li>a:hover,
					#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li>a:hover,
					#tie-body .tabs-menu .flexMenu-popup li a:hover{
						color: $dark_color;
					}

					#tie-body .woocommerce-tabs ul.tabs li.active a,
					#tie-body .tabs-widget .tabs-wrapper .tabs-menu li.is-active a {
					color: $bright;
						background-color: $color;
					}

					#tie-body .tabs-menu .flexMenu-popup {
						border-color: $color;
					}

					#tie-body .tabs-menu .flexMenu-popup li.is-active a {
						color: $bright;
					}
				";


				if( $block_style == 5 ){

					$out .="
						#tie-body .woocommerce-tabs ul.tabs>li.active a:before,
						#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:before,
						#tie-body .woocommerce-tabs ul.tabs>li.active:first-child a:after,
						#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li.is-active:first-child a:after,
						#tie-body .widget .tabs-widget .tabs-wrapper .tabs-menu li.is-active:last-child a:after {
							background-color: $color;
						}
					";
				}
				elseif( $block_style == 6 ){

					$out .= "
						#tie-body .woocommerce-tabs ul.tabs>li.active a:before,
						#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:before,
						#tie-body .woocommerce-tabs ul.tabs>li.active a:after,
						#tie-body .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:after {
							background-color: $color;
						}
					";
				}
			} // Magazine 2 if

		} // #4 || #5 || #6

		/* Style #7 */
		elseif( $block_style == 7 ){

			$out .= "
			#tie-body .section-title-default,
			#tie-body .mag-box-title,
			#tie-body .mag-box-options .mag-box-filter-links li a:hover,
			#tie-body #comments-title,
			#tie-body .comment-reply-title,
			#tie-body .review-box-header,
			#tie-body .woocommerce-tabs .panel h2,
			#tie-body .related.products h2:first-child,
			#tie-body .up-sells>h2,
			#tie-body .entry .cross-sells>h2,
			#tie-body .entry .cart_totals>h2,
			#tie-body #bbpress-forums #new-post>fieldset.bbp-form>legend{
				background-color: $color;
				color: $bright;
			}

			#tie-body .mag-box-options .mag-box-filter-links > li > a,
			#tie-body .mag-box-title h3 a,
			#tie-body .mag-box-title a.block-more-button,
			.dark-skin #tie-body .mag-box-title h3 a,
			#tie-body .box-dark-skin .mag-box-title a.block-more-button{
				color: $bright;
			}

			#tie-body .mag-box-options .mag-box-filter-links .flexMenu-viewMore > a:hover,
			#tie-body .mag-box-options .mag-box-filter-links .flexMenu-viewMore:hover > a,
			#tie-body .mag-box-options .mag-box-filter-links li li a.active{
				color: $color;
			}

			#tie-body .mag-box .container-wrapper .mag-box-options .mag-box-filter-links li a.active{
				color: rgba($rgb_bright_color, 0.8);
			}

			#tie-body .mag-box-options .mag-box-filter-links > li:not(.flexMenu-viewMore) > a:not(.active):hover,
			#tie-body .mag-box-options .mag-box-filter-links li > a.active{
				background-color: $bright !important;
				color: $color;
			}

			#tie-body .slider-arrow-nav a,
			.dark-skin #tie-body .mag-box .slider-arrow-nav a,
			#tie-body .box-dark-skin.mag-box .slider-arrow-nav a{
				border-color: rgba($rgb_bright_color,0.2);
				color: $bright;
			}

			#tie-body .mag-box-title a.pagination-disabled,
			#tie-body .mag-box-title a.pagination-disabled:hover{
				color: $bright !important;
			}

			#tie-body .mag-box-options .slider-arrow-nav a:not(.pagination-disabled):hover{
				background-color: $bright;
				border-color: $bright;
				color: $color;
			}
			";
		}


		/* Style #8 */
		elseif( $block_style == 8 ){

			$out .="
				#tie-body .section-title-default:before,
				#tie-body .mag-box-title h3:before,
				#tie-body .widget-title h4:before,
				#tie-body #comments-title:before,
				#tie-body .comment-reply-title:before,
				#tie-body .woocommerce-tabs .panel h2:before,
				#tie-body .related.products h2:first-child:before,
				#tie-body .up-sells>h2:before,
				#tie-body .entry .cross-sells>h2:before,
				#tie-body .entry .cart_totals>h2:before,
				#tie-body #bbpress-forums #new-post>fieldset.bbp-form>legend:before,
				#tie-body .review-box-header:before{
					background-color: $color;
				}
			";
		}


		return $out;
	}

}





/*-----------------------------------------------------------------------------------*/
# Set Sections Custom Styles
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_section_custom_styles' )){

	function jannah_section_custom_styles( $section ){

		if( empty( $section['settings']['section_id'] ) ){
			return false;
		}


		$section_css      = '';
		$section_styles   = array();
		$section_settings = $section['settings'];
		$section_id       = $section_settings['section_id'];


		# Margins ----------
		$section_styles[] = isset( $section_settings['margin_top'] )    ? 'margin-top:'.$section_settings['margin_top'].'px;'       : '';
		$section_styles[] = isset( $section_settings['margin_bottom'] ) ? 'margin-bottom:'.$section_settings['margin_bottom'].'px;' : '';

		$section_styles = implode( ' ', array_filter( $section_styles ) );

		if( ! empty( $section_styles )){
			$section_css .= "
				@media (min-width: 992px){
					#$section_id{
						$section_styles
					}
				}
			";
		}


		# Section Head Styles ----------
		if( ! empty( $section_settings['section_title'] ) && ! empty( $section_settings['title'] ) && ! empty( $section_settings['title_color'] )){

			$block_style = jannah_get_option( 'blocks_style', 1 );

			$color    = $section_settings['title_color'];
			$darker   = jannah_adjust_color_brightness( $color );
			$bright   = jannah_light_or_dark( $color );
			$selector = "#$section_id h2.section-title";


			# Centered Style ----------
			if( ! empty( $section_settings['title_style'] ) && $section_settings['title_style'] == 'centered' ){

				$section_css .= "

					$selector,
					$selector a{
						color: $color;
					}

					$selector a:hover{
						color: $darker;
					}

					$selector.section-title-centered:before,
					$selector.section-title-centered:after{
						background-color: $color;
					}
				";
			}

			# Big Style ----------
			elseif( ! empty( $section_settings['title_style'] ) && $section_settings['title_style'] == 'big' ){

				$section_css .= "

					$selector,
					$selector a{
						color: $color;
					}

					$selector a:hover{
						color: $darker;
					}
				";
			}

			# Default Style ----------
			elseif( empty( $section_settings['title_style'] ) ){

				$selector .= '.section-title-default';

				/* Style #1 */
				if( $block_style == 1 ){

					$section_css .= "
						$selector,
						$selector a{
							color: $color;
						}

						$selector a:hover{
							color: $darker;
						}

						$selector:before{
							border-top-color: $color;
						}

						$selector:after{
							background-color: $color;
						}
					";
				}

				/* Style #2 */
				if( $block_style == 2 ){

					$section_css .= "
						$selector,
						$selector a{
							border-color: $color;
							color: $color;
						}

						$selector a:hover{
							color: $darker;
						}
					";
				}

				/* Style #3 */
				elseif( $block_style == 3 ){

					$section_css .= "
						$selector,
						$selector a{
							color: $color;
						}

						$selector a:hover{
							color: $darker;
						}

						$selector:after {
							background: $color;
						}
					";
				}

				/* Style #4 || #5 || #6 */
				elseif( $block_style == 4 || $block_style == 5 || $block_style == 6 ){

					$section_css .= "
						$selector,
						$selector a{
							color: $bright;
						}

						$selector.section-title-default:before{
							background-color: $color;
						}
					";

					/* Style #6 */
					if( $block_style == 6 ){

						$section_css .= "
							$selector:after{
								background-color: $color;
							}
						";
					}
				}

				/* Style #7 */
				elseif( $block_style == 7 ){

					$section_css .= "
						$selector{
							background-color: $color;
							color: $bright;
						}

						$selector a{
							color: $bright;
						}

						$selector:after{
							background-color: $bright;
						}
					";
				}

				/* Style #8 */
				elseif( $block_style == 8 ){

					$section_css .= "
						$selector:before{
							background-color: $color;
						}

						$selector a:hover{
							color: $color;
						}
					";
				}

			}

			# Minify CSS ---------
			if( ! empty( $section_css )){
				return jannah_minify_css( $section_css );
			}

		}

	}
}




/*-----------------------------------------------------------------------------------*/
# Set Custom color for the blocks
/*-----------------------------------------------------------------------------------*/
if( ! function_exists( 'jannah_block_custom_color' )){

	function jannah_block_custom_color( $block ){

		if( empty( $block['color'] )) return;

		$id_css = '#tie-' .$block['boxid'];
		$color  = $block['color'];
		$bright = jannah_light_or_dark( $color );
		$darker = jannah_adjust_color_brightness( $color );
		$rgb_bright_color = jannah_get_rgb_color( $bright );

		$background_hover_elements = $color_elements = $background_elements = '';

		if( $block['style'] == 'woocommerce' ){
			$color_elements            .= ", $id_css.woocommerce a:hover, $id_css.woocommerce ins";
			$background_elements       .= ", $id_css .button";
			$background_hover_elements .= ", $id_css .button:hover";
		}

		if( $block['style'] == 'scroll' || $block['style'] == 'scroll_2' || $block['style'] == 'woocommerce' ){
			$background_elements .= ", $id_css .slick-dots li.slick-active button, $id_css .slick-dots li button:hover";
		}

		# General Custom block CSS code ----------
		$block_css = "
			$id_css .mag-box-title h3 a,
			$id_css.mag-box .container-wrapper .mag-box-title a.block-more-button,
			$id_css.mag-box .container-wrapper .mag-box-options .mag-box-filter-links li a.active,
			$id_css .stars-rating-active,
			$id_css .tabs-menu li.is-active a,
			$id_css .tabs-menu li.active > a,
			$id_css .mag-box-options .mag-box-filter-links li a.active:hover,
			$id_css .mag-box-options .mag-box-filter-links .flexMenu-viewMore > a:hover,
			$id_css .mag-box-options .mag-box-filter-links .flexMenu-viewMore:hover > a,
			$id_css .pages-nav li a:hover,
			$id_css .show-more-button:hover,
			$id_css .entry a,
			$id_css .spinner-circle:after,
			$id_css .video-playlist-nav-wrapper .video-playlist-item .video-play-icon
			$color_elements{
				color: $color;
			}

			$id_css a:hover,
			$id_css .mag-box-title h3 a:hover,
			$id_css a.block-more-button:hover{
				color: $darker;
			}

			$id_css .digital-rating-static strong,
			$id_css .spinner > div,
			$id_css .mag-box-options .slider-arrow-nav a:hover:not(.pagination-disabled),
			$id_css .mag-box-options .mag-box-filter-links li a:hover,
			$id_css .slick-dots li.slick-active button,
			$id_css .slick-dots li button:hover,
			$id_css li.current span
			$background_elements{
				background-color: $color;
			}

			$id_css a.more-link,
			$id_css .video-playlist-nav-wrapper .playlist-title,
			$id_css .breaking-title:before,
			$id_css .breaking-news-nav li:hover,
			$id_css .post-cat,
			$id_css .tie-slider-nav li > span:hover{
				background-color: $color;
				color: $bright;
			}

			$id_css a.post-cat:hover,
			$id_css a.more-link:hover
			$background_hover_elements{
				background-color: $darker;
				color: $bright !important;
			}

			$id_css .circle_bar{
				stroke: $color;
			}

			$id_css .mag-box-options .mag-box-filter-links li a:hover,
			$id_css .mag-box-options .slider-arrow-nav a:hover:not(.pagination-disabled){
				color: $bright;
			}

			$id_css .mag-box-options .slider-arrow-nav a:hover:not(.pagination-disabled),
			$id_css .mag-box-options .mag-box-filter-links li a:hover,
			$id_css li.current span,
			$id_css .breaking-news-nav li:hover{
				border-color: $color !important;
			}
		";

		# Blocks Head Styles ----------
		$block_style = jannah_get_option( 'blocks_style', 1 );

		/* Style #1 */
		if( $block_style == 1 ){

			$block_css .= "
				$id_css.mag-box .container-wrapper .mag-box-title{
					color: $color;
				}

				$id_css.mag-box .container-wrapper .mag-box-title:before {
					border-top-color: $color;
				}

				$id_css.mag-box .container-wrapper .mag-box-title:after{
					background-color: $color;
				}
			";
		}

		/* Style #2 */
		elseif( $block_style == 2 ){

			$block_css .= "
				$id_css.mag-box .container-wrapper .mag-box-title{
					border-color: $color;
					color: $color;
				}
			";
		}

		/* Style #3 */
		elseif( $block_style == 3 ){

			$block_css .= "
				$id_css.mag-box .container-wrapper .mag-box-title{
					color: $color;
				}

				$id_css.mag-box .container-wrapper .mag-box-title:after {
					background: $color;
				}
			";
		}

		/* Style #4 || #5 || #6 */
		elseif( $block_style == 4 || $block_style == 5 || $block_style == 6 ){

			$block_css .= "

				$id_css.mag-box .mag-box-title h3,
				$id_css.mag-box .mag-box-title h3 a,
				$id_css.box-dark-skin .mag-box-title h3 a{
					color: $bright;
				}

				$id_css.mag-box .mag-box-title h3:before{
					background-color: $color;
				}
			";

			/* Style #4 || #5 || #6 && Magazine 2 && Block Style == Tabs */
			if( jannah_get_option( 'boxes_style' ) == 2 && ( ! empty( $block['style'] ) && $block['style'] == 'tabs' )){

				$block_css .= "

					$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu {
						border-color: $color !important;
					}

					$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu li > a{
						color: $color;
					}

					$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu li a:hover{
						color: $darker;
					}

					$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu li.is-active a {
						color: $bright;
						background-color: $color;
					}

					$id_css.mag-box .tabs-menu .flexMenu-popup {
						border-color: $color;
					}

					$id_css.mag-box .tabs-menu .flexMenu-popup li.is-active a {
						color: $bright;
					}
				";
			}

			/* Style #5 && Magazine 2 */
			if( $block_style == 5 && jannah_get_option( 'boxes_style' ) == 2 ){

				$block_css .= "

					$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:before,
					$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu>li.is-active:first-child a:after{
						background-color: $color;
					}
				";
			}

			/* Style #6 */
			elseif( $block_style == 6 ){

				$block_css .= "

					$id_css.mag-box .mag-box-title h3:after{
						background-color: $color;
					}
				";

				/* Style #6 && Magazine 2 && Block Style == Tabs */
				if( jannah_get_option( 'boxes_style' ) == 2 && ! empty( $block['style'] ) && $block['style'] == 'tabs' ){

					$block_css .= "

						$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:before,
						$id_css.mag-box .tabs-widget .tabs-wrapper .tabs-menu>li.is-active a:after {
							background-color: $color;
						}
					";
				}
			} // #style 6

		} // #4 || #5 || #6

		/* Style #7 */
		elseif( $block_style == 7 ){

			$block_css .= "

				$id_css.mag-box .mag-box-title,
				$id_css.mag-box .mag-box-options .mag-box-filter-links li a:hover{
					background-color: $color;
					color: $bright;
				}

				$id_css.mag-box .mag-box-options .mag-box-filter-links > li > a,
				$id_css.mag-box .mag-box-title h3 a,
				$id_css.mag-box .container-wrapper .mag-box-title a.block-more-button,
				.dark-skin $id_css.mag-box .mag-box-title h3 a{
					color: $bright;
				}

				$id_css.mag-box .mag-box-options .mag-box-filter-links .flexMenu-viewMore > a:hover,
				$id_css.mag-box .mag-box-options .mag-box-filter-links .flexMenu-viewMore:hover > a,
				$id_css.mag-box .mag-box-options .mag-box-filter-links li li a.active{
					color: $color;
				}

				#tie-body $id_css.mag-box .container-wrapper .mag-box-options .mag-box-filter-links li a.active{
					color: rgba($rgb_bright_color, 0.8);
				}

				$id_css.mag-box .mag-box-options .mag-box-filter-links > li:not(.flexMenu-viewMore) > a:not(.active):hover,
				$id_css .mag-box-options .mag-box-filter-links > li > a.active{
					background-color: $bright !important;
					color: $color;
				}

				$id_css.mag-box .slider-arrow-nav a{
					border-color: rgba($rgb_bright_color,0.2);
					color: $bright;
				}

				$id_css.mag-box .mag-box-title a.pagination-disabled,
				$id_css.mag-box .mag-box-title a.pagination-disabled:hover{
					color: $bright !important;
				}

				$id_css.mag-box .mag-box-options .slider-arrow-nav a:not(.pagination-disabled){
					color: rgb($rgb_bright_color);
				}

				$id_css.mag-box .mag-box-options .slider-arrow-nav a:not(.pagination-disabled):hover{
					background-color: $bright;
					border-color: $bright !important;
					color: $color;
				}
			";
		}

		/* Style #8 */
		elseif( $block_style == 8 ){

			$block_css .= "

				$id_css.mag-box .mag-box-title h3:before{
					background-color: $color;
				}
			";
		}


		return jannah_minify_css( $block_css );

	}

}
