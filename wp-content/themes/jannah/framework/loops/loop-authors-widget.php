<li <?php jannah_post_class( 'widget-post-list' ); ?>>

	<?php

		$post_without_thumb = ' no-small-thumbs';

		# Show the avatar if it is active only ----------
		if( get_option( 'show_avatars' ) ){

			$post_without_thumb = ''; ?>
			<div class="post-widget-thumbnail" style="width:70px">
				<a class="author-avatar" href="<?php echo tie_get_author_profile_url(); ?>">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), 70 ); ?>
				</a>
			</div>
			<?php
		}

	?>

	<div class="comment-body post-widget-body<?php echo esc_attr( $post_without_thumb ) ?>">
		<h3 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a></h3>

		<?php jannah_the_post_meta( array( 'comments' => false, 'views' => false ) ); ?>
	</div>

</li>
