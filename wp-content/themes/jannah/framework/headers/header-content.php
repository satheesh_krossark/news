<div class="container">
	<div class="tie-row logo-row">

		<div class="logo-wrapper">
			<div class="tie-col-md-4 logo-container">
				<?php

					do_action( 'tie_before_logo' );

					jannah_logo();

					do_action( 'tie_after_logo' );

				?>
			</div><!-- .tie-col /-->
		</div><!-- .logo-wrapper /-->

		<?php

			# Get the Header AD ----------
			jannah_get_banner( 'banner_top', '<div class="tie-col-md-8 stream-item stream-item-top-wrapper"><div class="stream-item-top">', '</div></div><!-- .tie-col /-->' );

		?>

	</div><!-- .tie-row /-->
</div><!-- .container /-->
