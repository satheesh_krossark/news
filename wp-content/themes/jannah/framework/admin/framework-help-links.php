<?php
/**
 * Add Docs Links to the theme backend
 *
 * @package Jannah
 */


/*-----------------------------------------------------------------------------------*/
# Jannah Theme Documentation URLs
/*-----------------------------------------------------------------------------------*/
add_action( 'jannah-admin-after-tab-title',  'jannah_admin_add_help_icons', 10, 1 );
add_action( 'jannah-admin-after-head-title', 'jannah_admin_add_help_icons', 10, 1 );

function jannah_admin_add_help_icons( $id = false ){

	$options = array(

 		# General Settings ----------
 		'general-settings-tab' => '67-general-settings',
 		'time-format-settings' => '67-general-settings#time-format',
 		'breadcrumbs-settings' => '67-general-settings#breadcrumb',
 		'trim-text-settings'   => '67-general-settings#trim-text-settings',
 		'post-font-icon'       => '67-general-settings#post-format-icon-on-hover',

		# Layout ----------
 		'layout-tab' => '68-layout-settings',

		# Header Settings ----------
		'header-layout'       => '65-header-options#header-layouts',
		'main-nav-components' => '175-mian-nav-settings#mian-nav-components',
		'sticky-menu'         => '175-mian-nav-settings#sticky-menu',
		'top-nav-components'  => '174-secondary-nav-settings#secondary-nav-components',
		'breaking_news_head'  => '174-secondary-nav-settings#breaking-news',

		# Logo Settings ----------
 		'logo-settings-tab'  => '69-logo-settings',

		# Footer Settings ----------
 		'footer-settings-tab'  => '33-footer-settings',
 		'instagram-footer-area'=> '33-footer-settings#setting-up-the-instagram-footer-area',
 		'footer-widgets-layout'=> '33-footer-settings#how-to-add-widgets-to-the-footer',
 		'copyright-area'       => '33-footer-settings#copyright-area',
 		'back-to-top-button'   => '33-footer-settings#back-to-top-button',

 		# Archives ----------
 		'archives-settings-tab'            => '70-archives-settings',
 		'archives-default-layout-settings' => '70-archives-settings#default-layout-settings',

		# Single Post Page ----------
		'single-post-page-settings-tab' => '71-single-post-page',
		'default-posts-layout'          => '71-single-post-page#default-posts-layout',
		'structure-data'                => '71-single-post-page#structure-data',
		'post-general-settings'         => '71-single-post-page#general-settings',
		'post-info-settings'            => '71-single-post-page#post-info-settings',
		'post-newsletter'               => '71-single-post-page#newsletter',
		'related-posts'                 => '71-single-post-page#related-posts',
		'fly-check-also-box'            => '71-single-post-page#fly-check-also-box',

 		# Post Views System ----------
 		'post-views-tab'      => '170-post-views-settings',
 		'post-views-settings' => '170-post-views-settings#post-views-settings',

 		# Share Settings ----------
 		'share-settings-tab'     => '72-share-buttons',
 		'share-general-settings' => '72-share-buttons#general-settings',
 		'above-post-share'       => '72-share-buttons#above-post-share-buttons',
 		'select-and-share'       => '72-share-buttons#select-and-share',

 		# Sidebars Settings ----------
 		'sidebars-settings-tab' => '73-sidebars-settings',

 		# Lightbox Settings ----------
 		'lightbox-settings-tab' => '74-lightbox-settings',

 		# Advertisement Settings ----------
 		'advertisements-settings-tab' => '75-advertisement-settings',
 		'ad-blocker-detector'         => '75-advertisement-settings#ad-blocker-detector',
 		'background-image-ad'         => '75-advertisement-settings#background-image-ad',
 		'banner_top-ad'               => '75-advertisement-settings#header-e3lan',
 		'shortcodes-ads'              => '75-advertisement-settings#shortcodes-a3lans',

		# Background ----------
 		'background-tab'      => '76-site-background-settings',
 		'background-settings' => '76-site-background-settings',

 		# Styling Settings ----------
 		'styling-tab'           => '77-styling-settings',
 		'styling-settings'      => '77-styling-settings',
 		'custom-body-classes'   => '77-styling-settings#custom-body-classes',
 		'primary-color'         => '77-styling-settings#primary-color',
 		'body-styling'          => '77-styling-settings#body',
 		'secondary-nav-styling' => '77-styling-settings#secondary-nav',
 		'header-styling'        => '77-styling-settings#header',
 		'main-nav-styling'      => '77-styling-settings#main-nav-styling',
 		'main-content-styling'  => '77-styling-settings#main-content-styling',
 		'footer-styling'        => '77-styling-settings#footer-copyright-area',
 		'copyright-area-styling'=> '77-styling-settings#footer-copyright-area',
 		'mobile-menu-styling'   => '77-styling-settings#mobile-menu',
 		'custom-css'            => '77-styling-settings#custom-css',

 		# Typography Settings ----------
 		'typorgraphy-settings-tab'            => '78-typography-settings',
 		'google-web-font-character-sets'      => '78-typography-settings#google-web-font-character-sets',
 		'font-sizes-weights-and-line-heights' => '78-typography-settings#font-sizes-weights-and-line-heights',

		# Translations Settings ----------
 		'translations-settings-tab' => '79-translations-settings',

		# Social Networks ----------
 		'social-networks-tab'  => '80-social-networks',
 		'social-networks'      => '80-social-networks#social-networks',
 		'custom-social-network-1'=> '80-social-networks#custom-social-networks',
 		'custom-social-network-2'=> '80-social-networks#custom-social-networks',
 		'custom-social-network-3'=> '80-social-networks#custom-social-networks',
 		'custom-social-network-4'=> '80-social-networks#custom-social-networks',
 		'custom-social-network-5'=> '80-social-networks#custom-social-networks',

 		# Mobile Settings ----------
 		'mobile-settings-tab'    => '81-mobile-settings',
 		'mobile-settings'        => '81-mobile-settings#mobile-settings',
 		'mobile-menu'            => '81-mobile-settings#mobile-menu',
 		'mobile-single-post-page'=> '81-mobile-settings#single-post-page',
 		'mobile-elements'        => '81-mobile-settings#mobile-elements',
 		'sticky-mobile-share'    => '81-mobile-settings#sticky-mobile-share-buttons',

 		# AMP ----------
 		'accelerated-mobile-pages-tab' => '83-amp-accelerated-mobile-pages',
 		'accelerated-mobile-pages'     => '83-amp-accelerated-mobile-pages#accelerated-mobile-pages',
 		'amp-logo'                     => '83-amp-accelerated-mobile-pages#logo',
 		'amp-post-settings'            => '83-amp-accelerated-mobile-pages#post-settings',
 		'amp-footer-settings'          => '83-amp-accelerated-mobile-pages#footer-settings',
 		'amp-advertisement'            => '83-amp-accelerated-mobile-pages#advertisement',
 		'amp-styling'                  => '83-amp-accelerated-mobile-pages#styling',

 		# Web Notifications ----------
 		'web-notifications-tab'     => '84-web-notifications-settings',

 		# Advanced ----------
 		'advanced-settings-tab'     => '85-advanced-settings',
 		'advanced-settings'         => '85-advanced-settings#advanced-settings',
 		'wordpress-login-page-logo' => '85-advanced-settings#wordpress-login-page-logo',
 		'reset-all-settings'        => '85-advanced-settings#reset-all-settings',

 		# Web Notifications ----------
 		'woocommerce-tab'     => '86-woocommerce-settings',

		# bbPress ----------
		'bbpress-tab' => '87-bbpress-settings',

		# buddypress ----------
		'buddypress-tab' => '176-buddypress-settings',

	);

	add_thickbox();

	$docs_url = "https://jannah.helpscoutdocs.com/article/";

	if( ! empty( $id ) && ! empty( $options[ $id ] ) ){
		echo '<a href="'. esc_url( $docs_url . $options[ $id ] ) .'?tiedocs=true&TB_iframe=true&width=650&height=750" id="help-icon-'. $id .'" class="docs-link thickbox" target="_blank" title="'. esc_html__( 'Need Help?', 'jannah' ) .'"><span class="dashicons dashicons-editor-help"></span></a>';
	}

	return false;
}





/*-----------------------------------------------------------------------------------*/
# Arqam Lite Documentation URL
/*-----------------------------------------------------------------------------------*/
add_filter( 'arqam_lite_docs_url', 'jannah_arqam_lite_docs_url' );
function jannah_arqam_lite_docs_url( $url ) {
	return esc_url( 'https://jannah.helpscoutdocs.com/' );
}





/*-----------------------------------------------------------------------------------*/
# Jannah Extensions Shortcodes Documentation URL
/*-----------------------------------------------------------------------------------*/
add_filter( 'jannah_extensions_shortcodes_docs_url', 'jannah_extensions_shortcodes_docs_url' );
function jannah_extensions_shortcodes_docs_url( $url ) {
	return esc_url( 'https://jannah.helpscoutdocs.com/article/128-shortcodes' );
}





/*-----------------------------------------------------------------------------------*/
# Update the theme manually Documentation URL
/*-----------------------------------------------------------------------------------*/
add_filter( 'jannah_update_theme_manually_docs_url', 'jannah_update_theme_manually_docs_url' );
function jannah_update_theme_manually_docs_url( $url ) {
	return esc_url( 'https://jannah.helpscoutdocs.com/article/169-how-do-i-manually-update-the-theme-to-the-newer-version' );
}
