<?php

	jannah_theme_option(
		array(
			'title' => esc_html__( 'Post Views Settings', 'jannah' ),
			'id'    => 'post-views-tab',
			'type'  => 'tab-title',
		));

	jannah_theme_option(
		array(
			'type'  => 'header',
			'id'    => 'post-views-settings',
			'title' => esc_html__( 'Post Views Source', 'jannah' ),
		));

	jannah_theme_option(
		array(
			'name'    => esc_html__( 'Post Views Source', 'jannah' ),
			'id'      => 'tie_post_views',
			'type'    => 'select',
			'options' => array(
				''        => esc_html__( 'Disable', 'jannah' ),
				'theme'   => esc_html__( "Theme's module", 'jannah' ),
				'jetpack' => esc_html__( 'Jetpack plugin by Automattic', 'jannah' ),
				'plugin'  => esc_html__( 'Third party post views plugin', 'jannah' ),
			),
			'toggle'  => array(
				'theme'  => '#views_meta_field-item, #views_starter_number-item',
				'plugin' => '#views_meta_field-item',
			)
		));

	jannah_theme_option(
		array(
			'name'    => esc_html__( 'Post meta field', 'jannah' ),
			'id'      => 'views_meta_field',
			'type'    => 'text',
			'class'   => 'tie_post_views',
			'default' => 'tie_views',
			'hint'    => esc_html__( 'Chnage this if you have used a post views plugin before.', 'jannah' ),
		));

	jannah_theme_option(
		array(
			'name'    => esc_html__( 'Starter Post Views number', 'jannah' ),
			'id'      => 'views_starter_number',
			'type'    => 'number',
			'class'   => 'tie_post_views',
			'hint'    => esc_html__( 'This will applied on the new Posts only.', 'jannah' ),
		));

	jannah_theme_option(
		array(
			'type'  => 'header',
			'id'    => 'post-views-colored',
			'title' => esc_html__( 'Colored Post Views Numbers', 'jannah' ),
		));

	jannah_theme_option(
		array(
			'name'   => esc_html__( 'Colored Post Views Numbers', 'jannah' ),
			'id'     => 'views_colored',
			'toggle' => '#views_warm_color-item, #views_hot_color-item, #views_veryhot_color-item',
			'type'   => 'checkbox',
		));

	jannah_theme_option(
		array(
			'name' => sprintf( esc_html__( '%s Min. Views Number', 'jannah' ), '<span class="tie-number-views-badge" style="background: #f47512">'. esc_html__( 'WARM', 'jannah' ) .'</span>' ),
			'id'   => 'views_warm_color',
			'type' => 'number',
			'hint' => sprintf( esc_html__( 'Default is: %s', 'jannah' ), 500 ),
		));

	jannah_theme_option(
		array(
			'name' => sprintf( esc_html__( '%s Min. Views Number', 'jannah' ), '<span class="tie-number-views-badge" style="background: #f3502a">'. esc_html__( 'HOT', 'jannah' ) .'</span>' ),
			'id'   => 'views_hot_color',
			'type' => 'number',
			'hint' => sprintf( esc_html__( 'Default is: %s', 'jannah' ), 2000 ),
		));

	jannah_theme_option(
		array(
			'name' => sprintf( esc_html__( '%s Min. Views Number', 'jannah' ), '<span class="tie-number-views-badge" style="background: #f11e1e">'. esc_html__( 'VERY HOT', 'jannah' ) .'</span>' ),
			'id'   => 'views_veryhot_color',
			'type' => 'number',
			'hint' => sprintf( esc_html__( 'Default is: %s', 'jannah' ), 5000 ),
		));


