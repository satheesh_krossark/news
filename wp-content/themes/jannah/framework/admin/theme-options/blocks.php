<?php

	jannah_theme_option(
		array(
			'title' => esc_html__( 'Blocks Settings', 'jannah' ),
			'id'    => 'blocks-tab',
			'type'  => 'tab-title',
		));


	jannah_theme_option(
		array(
			'title' =>	esc_html__( 'Block Style', 'jannah' ),
			'type'  => 'header',
		));


	jannah_theme_option(
		array(
			'id'      => 'boxes_style',
			'type'    => 'visual',
			'options' => array(
				'1'	=> array( esc_html__( 'Bordered', 'jannah' ) => 'blocks/magazine-1.png' ),
				'2' => array( esc_html__( 'Clean', 'jannah' )    => 'blocks/magazine-2.png' ),
			)));

	jannah_theme_option(
		array(
			'title' =>	esc_html__( 'Block Head Style', 'jannah' ),
			'type'  => 'header',
		));


	jannah_theme_option(
		array(
			'id'      => 'blocks_style',
			'type'    => 'visual',
			'options' => array(
				'1'	=> array( esc_html__( 'Style', 'jannah' ) .' #1' => 'blocks/head-1.png' ),
				'2' => array( esc_html__( 'Style', 'jannah' ) .' #2' => 'blocks/head-2.png' ),
				'3' => array( esc_html__( 'Style', 'jannah' ) .' #3' => 'blocks/head-3.png' ),
				'4' => array( esc_html__( 'Style', 'jannah' ) .' #4' => 'blocks/head-4.png' ),
				'5' => array( esc_html__( 'Style', 'jannah' ) .' #5' => 'blocks/head-5.png' ),
				'6' => array( esc_html__( 'Style', 'jannah' ) .' #6' => 'blocks/head-6.png' ),
				'7' => array( esc_html__( 'Style', 'jannah' ) .' #7' => 'blocks/head-7.png' ),
				'8' => array( esc_html__( 'Style', 'jannah' ) .' #8' => 'blocks/head-8.png' ),
			)));


	# Global Blocks Meta Settings ----------
	jannah_theme_option(
		array(
			'title' =>	esc_html__( 'Blocks Meta Settings', 'jannah' ),
			'id'    =>	'blocks-meta-settings',
			'type'  => 'header',
		));

	jannah_theme_option(
		array(
			'name' => esc_html__( 'Disable Author name meta', 'jannah' ),
			'id'   => 'blocks_disable_author_meta',
			'type' => 'checkbox',
		));

	jannah_theme_option(
		array(
			'name' => esc_html__( 'Disable Comments number meta', 'jannah' ),
			'id'   => 'blocks_disable_comments_meta',
			'type' => 'checkbox',
		));

	jannah_theme_option(
		array(
			'name' => esc_html__( 'Disable Views Number meta', 'jannah' ),
			'id'   => 'blocks_disable_views_meta',
			'type' => 'checkbox',
		));
