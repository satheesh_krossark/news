<?php

	jannah_theme_option(
		array(
			'title' => esc_html__( 'Custom Menu', 'jannah' ),
			'type'  => 'header',
		));

	jannah_custom_post_option(
		array(
			'name'    => esc_html__( 'Custom Menu', 'jannah' ),
			'id'      => 'tie_menu',
			'type'    => 'select',
			'options' => jannah_get_menus_array( true ),
		));

	/*
	jannah_theme_option(
		array(
			'title' => esc_html__( 'Logo in the Sticky Menu', 'jannah' ),
			'type'  => 'header',
		));

	if( ! jannah_get_option('stick_nav') ){

		jannah_custom_post_option(
			array(
				'text' => esc_html__( 'You need to enable The Sticky Menu option from the theme options page &gt; Header Settings &gt; Sticky Menu to use these options.', 'jannah' ),
				'type' => 'message',
			));
	}

	else{
		jannah_custom_post_option(
			array(
				'name'   => esc_html__( 'Sticky Menu Logo', 'jannah' ),
				'id'     => 'sticky_logo_type',
				'type'   => 'radio',
				'toggle' => array(
					'none'    => '',
					'default' => '',
					'custom'  => '#sticky-logo-options',),
				'options' => array(
					''        => esc_html__( 'Default', 'jannah' ),
					'none'    => esc_html__( 'Disable', 'jannah' ),
					'default' => esc_html__( 'Use the default Logo', 'jannah' ),
					'custom'  => esc_html__( 'Custom Sticky Logo', 'jannah' ) . ' <small style="color: red;">'. esc_html__( ' - Requries a Custom Logo for the page, you cns set from the Logo tab.', 'jannah' ) .'</small>',
				)));

		echo '<div id="sticky-logo-options" class="sticky_logo_type-options">';

			jannah_custom_post_option(
				array(
					'name'  => esc_html__( 'Logo Image', 'jannah' ),
					'id'    => 'logo_sticky',
					'type'  => 'upload',
				));

			jannah_custom_post_option(
				array(
					'name'  => esc_html__( 'Logo Image (Retina Version @2x)', 'jannah' ),
					'id'    => 'logo_retina_sticky',
					'type'  => 'upload',
					'hint'	=> esc_html__( 'Please choose an image file for the retina version of the logo. It should be 2x the size of main logo.', 'jannah' ),
				));

		echo'</div>';
	}
	*/
