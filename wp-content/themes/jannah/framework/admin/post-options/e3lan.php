<?php

	jannah_theme_option(
		array(
			'title' => esc_html__( 'Advertisement', 'jannah' ),
			'type'  => 'header',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Hide Above Post Ad', 'jannah' ),
			'id'   => 'tie_hide_above',
			'type' => 'checkbox',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Custom Above Post Ad', 'jannah' ),
			'id'   => 'tie_get_banner_above',
			'type' => 'textarea',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Hide Below Post Ad', 'jannah' ),
			'id'   => 'tie_hide_below',
			'type' => 'checkbox',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Custom Below Post Ad', 'jannah' ),
			'id'   => 'tie_get_banner_below',
			'type' => 'textarea',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Hide Above Content Ad', 'jannah' ),
			'id'   => 'tie_hide_above_content',
			'type' => 'checkbox',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Custom Above Content Ad', 'jannah' ),
			'id'   => 'tie_get_banner_above_content',
			'type' => 'textarea',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Hide Below Content Ad', 'jannah' ),
			'id'   => 'tie_hide_below_content',
			'type' => 'checkbox',
	));

	jannah_custom_post_option(
		array(
			'name' => esc_html__( 'Custom Below Content Ad', 'jannah' ),
			'id'   => 'tie_get_banner_below_content',
			'type' => 'textarea',
	));
