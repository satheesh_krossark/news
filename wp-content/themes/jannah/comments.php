<?php
/**
 * The template for displaying comments
 *
 * @package Jannah
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ){
	return;
}

if ( have_comments() || comments_open() ) : ?>
	<div id="comments" class="comments-area">

		<?php if ( have_comments() ) : ?>
			<div id="comments-box" class="container-wrapper">

				<div class="block-head">
					<h3 id="comments-title" class="the-global-title">
						<?php

							$comments_number = get_comments_number();
							if ( $comments_number > 1 ){
								printf(
									_ti( '%s Comments' ),
									number_format_i18n( $comments_number )
								);
							}
							else {
								_eti( 'One Comment', 'jannah' );
							}

						?>
					</h3>
				</div><!-- .block-head /-->

				<?php the_comments_navigation(); ?>

				<ol class="comment-list">
					<?php
						wp_list_comments( array(
							'style'       => 'ol',
							'short_ping'  => true,
							'avatar_size' => 70,
						) );
					?>
				</ol><!-- .comment-list -->

				<?php the_comments_navigation(); ?>

			</div><!-- #comments-box -->
		<?php endif; // Check for have_comments(). ?>


		<?php comment_form( array( 'title_reply_before' => '<h3 id="reply-title" class="comment-reply-title the-global-title">' ) ); ?>

	</div><!-- .comments-area -->

<?php endif; // Check for have_comments() || comments_open() ?>
