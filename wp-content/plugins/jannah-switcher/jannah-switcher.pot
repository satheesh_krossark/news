#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Jannah Switcher\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-16 07:14+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. Author URI of the plugin
msgid "https://tielabs.com/"
msgstr ""

#. Name of the plugin
msgid "Jannah Switcher"
msgstr ""

#. Description of the plugin
msgid "Switch your current posts to Jannah Theme"
msgstr ""

#. Author of the plugin
msgid "TieLabs"
msgstr ""
