<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'news');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E.Oc{}wXg(4F1eF1;q@kKDW(D(5Z_MJyj#>d!5kSxc|=lYW.,:[:CgAobomE[TqM');
define('SECURE_AUTH_KEY',  'C.9%NVj!LC_lh E4(OL@MM*NMi4rm]jbJ^bZA8S?uc,dQky0vqLU!`/V=8TBW0/+');
define('LOGGED_IN_KEY',    'm-BlU##0[_>kt_:|3^YnCP/FvE;4d*=;xlSvR_ao_$cJcu|ywq43WBW8):nt:PCS');
define('NONCE_KEY',        'Y8V^&+qs54x&K,N&>UR![t4!^Oc+ksf`Si?tLeeX?+9HI;B/ToFte,:#LNvW>7hF');
define('AUTH_SALT',        '|LT}M5r8WmUl1gq{bP=2{:jESrN9pmso>|op3#(l&h/,8wa1!D;M|FWxxI:oEF.*');
define('SECURE_AUTH_SALT', 'wfxjvV>k*qG6urQwnGps;LPVH8STKdEQA25$Hkb^C!QY4avFzbe#4ErM/|Tlp4+~');
define('LOGGED_IN_SALT',   'T[a(hyVya/j#gFR92jn63L)$L&x% l:Q/AAq22$=z!p{)()&}26!62?cp-6?#oem');
define('NONCE_SALT',       'z`!W@J6MN,Hvv]/v.(sJL%B[AJT/8Nw`4;xt.,X}61qgI<(?x lc]iH$f]VHTqx!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
